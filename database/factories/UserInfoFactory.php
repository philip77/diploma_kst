<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserInfo>
 */
class UserInfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $gender = ['male', 'female'];
        $genderIndex = random_int(0, 1);
        return [
            'user_id' => User::factory(),
            'first_name' => fake()->name,
            'middle_name' => fake()->name,
            'last_name' => fake()->name,
            'gender' => $gender[$genderIndex],
            'birth_date' => '08/14/1984'
        ];
    }
}
