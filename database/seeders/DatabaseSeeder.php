<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\CommentAnswers;
use App\Models\Comments;
use App\Models\HistoricalSites;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\UserInfo;
use Database\Factories\CommentsFactory;
use Database\Factories\HistoricalSitesFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        UserInfo::factory(10)->create();
        HistoricalSites::factory(10)->create();

        $counter = 1;
        foreach (HistoricalSites::all() as $historicalSite) {
            $user = User::where('id', $counter)->get();
//            $historicalSite->users()->attach($user);
//            $historicalSite->usersScore()->attach($user);
            Comments::factory(1)->create(['historical_site_id' => $historicalSite->id, 'user_id' => $counter]);
            CommentAnswers::factory(1)->create(['comment_id' => $counter, 'user_id' => $counter]);
            $counter++;
        }

        $role = Role::factory(1)->create(['name' => 'Admin']);

        $permissionsArray = [
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'index'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'showUsers'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'setUserRole'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'updateUserRole'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'createRole'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'storeRole'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'createPermissionRole'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'updatePermissionRole'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'showHistoricalSites'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'showHistoricalSite'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'createHistoricalSite'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'storeHistoricalSite'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'editHistoricalSite'],
            ['controller' => 'App\Http\Controllers\Admin\AdminController', 'action' => 'updateHistoricalSite'],
        ];

        foreach ($permissionsArray as $permission) {
            $permission = Permission::factory(1)->create(['controller' => $permission['controller'], 'action' => $permission['action']]);
            $query = 'INSERT INTO permission_role (permission_id, role_id, created_at, updated_at) VALUES (?, ?, ?, ?)';
            $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');
            DB::insert($query, [$permission[0]->id, $role[0]->id, $dateTimeNow, $dateTimeNow]);
        }

        $user = User::factory(1)->create(['username' => 'admin', 'email' => 'admin@admin.eu', 'password' => Hash::make('test1234')]);
        $query = 'INSERT INTO user_infos (user_id, created_at, updated_at) VALUES (?, ?, ?)';
        $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');
        DB::insert($query, [$user[0]->id, $dateTimeNow, $dateTimeNow]);

        $query = 'INSERT INTO role_user (role_id, user_id, created_at, updated_at) VALUES (?, ?, ?, ?)';
        $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');
        DB::insert($query, [$role[0]->id, $user[0]->id, $dateTimeNow, $dateTimeNow]);

        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
