<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comment_like_dislike_data', function (Blueprint $table) {
            $table->id();
            $table->foreignId('comment_answer_id')->nullable()->constrained();
            $table->foreignId('comment_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->enum('comment_type', ['comment', 'answer'])->nullable();
            $table->enum('comment_value', ['like', 'dislike', 'deleted']);
            $table->timestamps();

            $table->unique(['comment_answer_id', 'comment_id', 'user_id', 'comment_type'], 'like_comment_answer_comment_user_comment_unique');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comment_like_dislike_data');
    }
};
