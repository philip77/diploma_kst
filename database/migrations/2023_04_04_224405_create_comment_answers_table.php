<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comment_answers', function (Blueprint $table) {
            $table->id();
            $table->longText('answer')->nullable();
            $table->foreignId('comment_id')->constrained();
            $table->foreignId('user_id')->constrained();
            $table->integer('likes')->default(0);
            $table->integer('dislikes')->default(0);
            $table->boolean('deleted')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comment_answers');
    }
};
