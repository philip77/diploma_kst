<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentLikeDislikeDataStoreRequest;
use App\Services\CommentLikeDislikeDataService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class CommentLikeDislikeDataController extends Controller
{
    public function storeCommentLike(CommentLikeDislikeDataStoreRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();
            $likeDislikeData = (new CommentLikeDislikeDataService())->process($validateData);

            return Response::json(['success' => 'Successfully add like/dislike for comment!', ['likeDislikeData' => $likeDislikeData]], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }
}
