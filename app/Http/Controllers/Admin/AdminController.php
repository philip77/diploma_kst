<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddUserRoleRequest;
use App\Http\Requests\HistoricalSiteRequest;
use App\Http\Requests\RolePermissionStoreRequest;
use App\Http\Requests\RoleStoreRequest;
use App\Models\HistoricalSites;
use App\Models\Role;
use App\Models\User;
use App\Services\AdminService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class AdminController extends Controller
{

    public function __construct(private AdminService $adminService)
    {
    }

    public function index(): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'index']]);

            return view('admin.index');
        } catch (\Exception $e) {
            return Redirect::to('dashboard')->with('error', $e->getMessage());
        }
    }

    public function showUsers(): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'showUsers']]);
            $users = User::with('userInfo');
            $searchEmail = request('search_email');

            if ($searchEmail) {
                $whereClause[] = ['email', 'like', "%{$searchEmail}%"];
                $users->where($whereClause);
            }

            return view('admin.show-users', ['users' => $users->orderBy('id')->paginate(5)->withQueryString()]);
        } catch (\Exception $e) {
            return Redirect::to('dashboard')->with('error', $e->getMessage());
        }
    }

    public function setUserRole(User $user): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'setUserRole']]);

            $userWithRoles = $user::with('roles')->where('id', $user->id)->first();
            $rolesForUser = $userWithRoles->roles->first();

            return view('admin.set-user-role', [
                    'roles' => Role::all(),
                    'userData' => [
                        'userId' => $user->id,
                        'userRoleName' => $rolesForUser?->name
                    ]
                ]
            );
        } catch (\Exception $e) {
            return Redirect::to('dashboard')->with('error', $e->getMessage());
        }
    }

    public function updateUserRole(AddUserRoleRequest $request, User $user): RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'updateUserRole']]);

            $validateData = $request->validated();
            $roleId = isset($validateData['role']) ? (int) $validateData['role'] : 0;

            $this->adminService->addUserRole($user, $roleId);

            return Redirect::route('admin.set.user.role', [$user->id])->with('status', 'role-updated')->with('success', 'You successfully change user role!');
        } catch (\Exception $e) {
            return Redirect::route('admin.set.user.role', [$user->id])->with('error', $e->getMessage());
        }
    }

    public function createRole(): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'createRole']]);

            $roles = Role::orderBy('id')->paginate(10)->withQueryString();

            return view('admin.create-role', ['roles' => $roles]);
        } catch (\Exception $e) {
            return Redirect::route('admin.index')->with('error', $e->getMessage());
        }
    }

    public function storeRole(RoleStoreRequest $request): RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'storeRole']]);

            $validateData = $request->validated();

            Role::create($validateData);

            return Redirect::route('admin.create.role')->with('status', 'role-store')->with('success', 'You successfully create user role!');
        } catch (\Exception $e) {
            return Redirect::route('admin.create.role')->with('error', $e->getMessage());
        }
    }

    public function createPermissionRole(Role $role): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'createPermissionRole']]);

            $methodsInAdminControllerFiltered = $this->adminService->getAdminControllerMethodsData();
            $roleWithPermission = Role::with('permissions')->where('id', $role->id)->first();

            return view('admin.create-permission-role', [
                'role' => $roleWithPermission,
                'class' => AdminController::class,
                'methodsInAdminControllerFiltered' => $methodsInAdminControllerFiltered
            ]);
        } catch (\Exception $e) {
            return Redirect::route('admin.index')->with('error', $e->getMessage());
        }
    }

    public function updatePermissionRole(RolePermissionStoreRequest $request, Role $role): RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'updatePermissionRole']]);

            $validateData = $request->validated();

            $class = isset($validateData['class']) ? $validateData['class'] : '';
            $actions = isset($validateData['actions']) ? $validateData['actions'] : [];

            $this->adminService->managedPermissionRoles($class, $actions, $role);


            return Redirect::route('admin.create.permission.role', [$role->id])->with('status', 'role-permission-store')->with('success', 'You successfully add permissions to role!');
        } catch (\Exception $e) {
            return Redirect::route('admin.create.permission.role', [$role->id])->with('error', $e->getMessage());
        }
    }

    public function showHistoricalSites(): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'showHistoricalSites']]);
            $historicalSites = HistoricalSites::orderBy('id');

            $searchName = request('search_name');

            if ($searchName) {
                $whereClause[] = ['name', 'like', "%{$searchName}%"];
                $historicalSites->where($whereClause);
            }

            return view('admin.historical-sites', ['historicalSites' => $historicalSites->paginate(5)->withQueryString()]);
        } catch (\Exception $e) {
            return Redirect::route('dashboard')->with('error', $e->getMessage());
        }
    }

    public function showHistoricalSite(HistoricalSites $historicalSite): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'showHistoricalSite']]);

            return view('admin.historical-site', ['historicalSite' => $historicalSite]);
        } catch (\Exception $e) {
            return Redirect::route('dashboard')->with('error', $e->getMessage());
        }
    }

    public function createHistoricalSite(): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'createHistoricalSite']]);

            return view('admin.create-historical-site');
        } catch (\Exception $e) {
            return Redirect::route('admin.show.historical.sites')->with('error', $e->getMessage());
        }
    }

    public function storeHistoricalSite(HistoricalSiteRequest $request): RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'storeHistoricalSite']]);

            $validateData = $request->validated();

            unset($validateData['historical_site_image']);
            $historicalSite = HistoricalSites::create($validateData);

            if ($request->hasFile('historical_site_image')) {
                $historicalSite->addMediaFromRequest('historical_site_image')->toMediaCollection('historicalSiteImage');
            }

            return Redirect::route('admin.show.historical.sites')->with('status', 'historical-site-store')->with('success', 'You successfully create historical site!');
        } catch (\Exception $e) {
            return Redirect::route('admin.create.historical.site')->with('error', $e->getMessage());
        }
    }

    public function editHistoricalSite(HistoricalSites $historicalSite): View|RedirectResponse
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'editHistoricalSite']]);

            return view('admin.edit-historical-site', ['historicalSite' => $historicalSite]);
        } catch (\Exception $e) {
            return Redirect::route('admin.show.historical.sites')->with('error', $e->getMessage());
        }
    }

    public function updateHistoricalSite(HistoricalSiteRequest $request, HistoricalSites $historicalSite)
    {
        try {
            $this->authorize('index', [Role::class, ['controller' => AdminController::class, 'action' => 'updateHistoricalSite']]);
            $validateData = $request->validated();

            if ($request->hasFile('historical_site_image')) {
                $historicalSite->clearMediaCollection('historicalSiteImage');
                $historicalSite->addMediaFromRequest('historical_site_image')->toMediaCollection('historicalSiteImage');
            }

            unset($validateData['historical_site_image']);
            $historicalSite->update($validateData);

            return Redirect::route('admin.show.historical.site', [$historicalSite->id])->with('status', 'historical-site-update')->with('success', 'You successfully update update historical site information!');
        } catch (\Exception $e) {
            return Redirect::route('admin.edit.historical.site', [$historicalSite->id])->with('error', $e->getMessage());
        }
    }
}
