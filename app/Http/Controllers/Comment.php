<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentDestroyRequest;
use App\Http\Requests\CommentStoreRequest;
use App\Models\Comments;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class Comment extends Controller
{
    public function storeComment(CommentStoreRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();
            $validateData['user_id'] = auth()->user()->id;
            $validateData['likes'] = 0;
            $validateData['dislikes'] = 0;

            $comment = Comments::create($validateData);

            return Response::json(['success' => 'Successfully add comment!', 'comment' => $comment], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }

    public function destroyComment(CommentDestroyRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();

            Comments::where('id', $validateData['id'])->update(['deleted' => 1]);

            return Response::json(['success' => 'Successfully delete comment!'], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }
}
