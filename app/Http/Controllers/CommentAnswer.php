<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentAnswersDestroyRequest;
use App\Http\Requests\CommentAnswerStoreRequest;
use App\Models\CommentAnswers;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class CommentAnswer extends Controller
{
    public function storeCommentAnswer(CommentAnswerStoreRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();
            $validateData['user_id'] = auth()->user()->id;
            $validateData['likes'] = 0;
            $validateData['dislikes'] = 0;

            $commentAnswer = CommentAnswers::create($validateData);

            return Response::json(['success' => 'Successfully add answer for comment!', 'commentAnswer' => $commentAnswer], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }

    public function destroyCommentAnswer(CommentAnswersDestroyRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();

            CommentAnswers::where('id', $validateData['id'])->update(['deleted' => 1]);

            return Response::json(['success' => 'Successfully delete answer!'], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }
}
