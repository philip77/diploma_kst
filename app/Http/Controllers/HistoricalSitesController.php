<?php

namespace App\Http\Controllers;

use App\Http\Requests\HistoricalSiteScoreRequest;
use App\Http\Requests\HistoricalSiteUserRequest;
use App\Models\HistoricalSites;
use App\Services\HistoricalSiteScoreService;
use App\Services\HistoricalSiteService;
use App\Services\HistoricalSiteUserService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

class HistoricalSitesController extends Controller
{
    public function __construct(private HistoricalSiteService $historicalSiteService,
                                private HistoricalSiteUserService $historicalSiteUserService,
                                private HistoricalSiteScoreService $historicalSiteScoreService)
    {
    }

    public function index(): View|RedirectResponse
    {
        if (Auth::check()) {
            return Redirect::to('dashboard');
        }

        $whereClause = [];
        $whereClause[] = ['confirmed', '=', 1];
        $searchName = request('search_name');

        if ($searchName) {
            $whereClause[] = ['name', 'like', "%{$searchName}%"];
        }

        return view('historical_sites.not_authenticated.index', [
            'historicalSites' => HistoricalSites::where($whereClause)->latest('id')->paginate(3)->withQueryString()
        ]);
    }

    public function show(HistoricalSites $historicalSite): View|RedirectResponse
    {
        if (Auth::check()) {
            return Redirect::to('/historical-site-show/' . $historicalSite->id);
        }

        return view('historical_sites.not_authenticated.show', ['historicalSite' => $historicalSite]);
    }

    public function dashboard(): View
    {
        $whereClause = [];
        $whereClause[] = ['confirmed', '=', 1];
        $searchName = request('search_name');

        if ($searchName) {
            $whereClause[] = ['name', 'like', "%{$searchName}%"];
        }

        return view('historical_sites.dashboard', [
            'historicalSites' => HistoricalSites::where($whereClause)->latest('id')->paginate(3)->withQueryString()
        ]);
    }

    public function historicalSite(HistoricalSites $historicalSite): View
    {
        $comments = $this->historicalSiteService->getComments($historicalSite);
        $usersData = $this->historicalSiteService->getUsersData($comments);
        $isVisited = $this->historicalSiteService->isVisited($historicalSite);
        $ratingData = $this->historicalSiteService->getRating($historicalSite);

        return view('historical_sites.show', [
            'historicalSite' => $historicalSite,
            'comments' => $comments,
            'usersData' => $usersData,
            'isVisited' => $isVisited,
            'ratingData' => $ratingData
        ]);
    }

    public function visitedHistoricalSites(): View
    {
        $visitedHistoricalSites = $this->historicalSiteService->getVisitedHistoricalSites();
        $countVisitedHistoricalSites = $visitedHistoricalSites->count();

        $searchName = request('search_name');

        if ($searchName) {
            $whereClause[] = ['name', 'like', "%{$searchName}%"];
            $visitedHistoricalSites->where($whereClause);
        }

        $countHistoricalSites = HistoricalSites::where('confirmed', 1)->count();
        $percentVisitedHistoricalSites = round(($countVisitedHistoricalSites * 100 / $countHistoricalSites), 2);

        return view('historical_sites.visited', [
            'historicalSites' => $visitedHistoricalSites->latest('id')->paginate(3)->withQueryString(),
            'countHistoricalSites' => $countHistoricalSites,
            'countVisitedHistoricalSites' => $countVisitedHistoricalSites,
            'percentVisitedHistoricalSites' => $percentVisitedHistoricalSites
        ]);
    }

    public function visitedHistoricalSite(HistoricalSiteUserRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();
            $historicalSiteData = $this->historicalSiteUserService->process($validateData);

            return Response::json(['success' => 'Successfully add is visited flag for historical site!', ['historicalSiteData' => $historicalSiteData]], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }

    public function ratingHistoricalSite(HistoricalSiteScoreRequest $request): JsonResponse
    {
        try {
            $validateData = $request->validated();
            $historicalSiteScoreData = $this->historicalSiteScoreService->process($validateData);

            return Response::json(['success' => 'Successfully set score for historical site!', ['historicalSiteScoreData' => $historicalSiteScoreData]], 201);
        } catch (\Exception $e) {
            return Response::json(['error' => $e->getMessage()], 400);
        }
    }
}
