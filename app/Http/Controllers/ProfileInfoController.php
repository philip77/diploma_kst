<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileInfoUpdateRequest;
use App\Models\UserInfo;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class ProfileInfoController extends Controller
{
    public function update(ProfileInfoUpdateRequest $request, UserInfo $userInfo): RedirectResponse
    {
        try {
            $validateData = $request->validated();
            if ($request->hasFile('profile_image')) {
                $userInfo->clearMediaCollection('profileImage');
                $userInfo->addMediaFromRequest('profile_image')->toMediaCollection('profileImage');
            }

            unset($validateData['profile_image']);
            $userInfo->update($validateData);

            return Redirect::route('profile.edit')->with('status', 'profile-updated')->with('success', 'You successfully update additions profile information!');
        } catch (\Exception $e) {
            return Redirect::route('profile.edit')->with('error', 'Problem in the system. You could not update your profile information.');
        }
    }
}
