<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HistoricalSiteScoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'historical_site_id' => ['required', Rule::exists('historical_sites', 'id')],
            'user_id' => ['required', Rule::exists('users', 'id')->where(function ($query) {
                return $query->where('id', auth()->user()->id);
            })],
            'rating' => ['required', Rule::in(['1', '2', '3', '4', '5'])]
        ];
    }
}
