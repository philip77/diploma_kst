<?php

namespace App\Http\Requests;

use App\Enum\Gender;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class ProfileInfoUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['max:255'],
            'middle_name' => ['max:255'],
            'last_name' => ['max:255'],
            'gender' => ['nullable', new Enum(Gender::class)],
            'profile_image' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:2048'],
            'birth_date' => ['nullable', 'date']
        ];
    }
}
