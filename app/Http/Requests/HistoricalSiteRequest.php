<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HistoricalSiteRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', Rule::unique('roles', 'name')->ignore('id')],
            'data' => ['nullable'],
            'confirmed' => [Rule::in(['0', '1'])],
            'historical_site_image' => ['image', 'mimes:jpg,png,jpeg,gif,svg', 'max:5048'],
        ];
    }
}
