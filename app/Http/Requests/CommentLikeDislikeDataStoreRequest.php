<?php

namespace App\Http\Requests;

use App\Enum\CommentType;
use App\Enum\CommentValue;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CommentLikeDislikeDataStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'comment_answer_id' => ['nullable', Rule::exists('comment_answers', 'id')],
            'comment_id' => [Rule::exists('comments', 'id')],
            'user_id' => Rule::exists('users', 'id')->where(function ($query) {
                return $query->where('id', auth()->user()->id);
            }),
            'comment_value' => [new Enum(CommentValue::class)],
            'comment_type' => [new Enum(CommentType::class)],
        ];
    }
}
