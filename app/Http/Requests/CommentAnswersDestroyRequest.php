<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CommentAnswersDestroyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'id' => Rule::exists('comment_answers')->where(function ($query) {
                return $query->where('id', request()->get('id'))->where('user_id', auth()->user()->id);
            })
        ];

    }
}
