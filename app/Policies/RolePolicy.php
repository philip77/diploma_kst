<?php

namespace App\Policies;

use App\Models\Role;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class RolePolicy
{
    public function index(User $user, $data): bool
    {
        $result = false;
        $userWithRoles = $user::with('roles')->where('id', $user->id)->first();
        $rolesForUser = $userWithRoles->roles->first();
        $permissions = isset($rolesForUser->permissions) ? $rolesForUser->permissions : [];

        foreach ($permissions as $permission) {
            if ($permission->controller == $data['controller'] && $permission->action == $data['action']) {
                $result = true;
                break;
            }
        }

        return $result;
    }
}
