<?php

namespace App\Services;

use App\Http\Controllers\Admin\AdminController;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AdminService
{
    public function addUserRole(User $user, int $roleId): bool|\Exception
    {
        $userWithRoles = User::where('id', $user->id)->with('roles')->first();
        $userRoleRow = $userWithRoles->roles->first();

        if ($roleId === 0 && $userRoleRow?->role_user) {
            $userRoleRow->role_user->delete();
            return true;
        }

        $role = Role::where('id', $roleId)->first();

        if (! $role) {
            throw new \Exception('Invalid role!');
        }

        if (!$userRoleRow) {
            $query = 'INSERT INTO role_user (role_id, user_id, created_at, updated_at) VALUES (?, ?, ?, ?)';

            $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');

            if (! DB::insert($query, [$roleId, $user->id, $dateTimeNow, $dateTimeNow])) {
                throw new \Exception('Could not insert row in role_user!');
            }
        } else {
            $userRoleRow->role_user->update(['role_id' => $roleId, 'user_id' => $user->id]);
        }

        return true;
    }

    public function getAdminControllerMethodsData(): array
    {
        $methodsInAdminController = (new \ReflectionClass(AdminController::class))->getMethods();

        return  array_filter($methodsInAdminController, function ($methodData){
            return !str_contains($methodData->name, '__') && str_contains($methodData->class, 'AdminController');
        });
    }

    public function managedPermissionRoles(string $class, array $actions, Role $role): bool
    {
        $roleWithPermissions = Role::with('permissions')->where('id', $role->id)->first();
        $notExistPermissionRoles = [];

        foreach ($actions as $action) {
            $permission = Permission::where(['controller' => $class, 'action' => $action])->first();

            if (!$permission) {
                $permission = $this->storePermission($class, $action);
            }

            $existPermissionRole = false;
            foreach ($roleWithPermissions->permissions as $permissionWithRole) {
                if ($permissionWithRole->permission_role->role_id == $role->id && $permissionWithRole->permission_role->permission_id == $permission->id) {
                    $existPermissionRole = true;
                }
            }

            if (! $existPermissionRole) {
                $notExistPermissionRoles[] = [
                    'role_id' => $role->id,
                    'permission_id' => $permission->id
                ];
            }
        }

        $removePermissionsRoles = [];
        foreach ($roleWithPermissions->permissions as $permissionWithRole) {
            $notExistPermissionRole = true;
            foreach ($actions as $action) {
                $permission = Permission::where(['controller' => $class, 'action' => $action])->first();
                if ($permissionWithRole->permission_role->role_id == $role->id && $permissionWithRole->permission_role->permission_id == $permission->id) {
                    $notExistPermissionRole = false;
                }
            }

            if ($notExistPermissionRole) {
                $removePermissionsRoles[] = [
                    'role_id' => $permissionWithRole->permission_role->role_id,
                    'permission_id' => $permissionWithRole->permission_role->permission_id
                ];
            }
        }

        $this->storePermissionRole($notExistPermissionRoles);
        $this->removePermissionRole($removePermissionsRoles);

        return true;
    }

    private function storePermission(string $class, string $action): Permission
    {
        return Permission::create(['controller' => $class, 'action' => $action]);
    }

    private function storePermissionRole(array $data): bool
    {
        $query = 'INSERT INTO permission_role (role_id, permission_id, created_at, updated_at) VALUES (?, ?, ?, ?)';
        $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');
        foreach ($data as $row) {
            if (! DB::insert($query, [$row['role_id'], $row['permission_id'], $dateTimeNow, $dateTimeNow])) {
                throw new \Exception('Could not insert row in permission_role!');
            }
        }

        return true;
    }

    private function removePermissionRole(array $data): bool
    {
        foreach ($data as $row) {
            if (! DB::table('permission_role')->where(['role_id' => $row['role_id'], 'permission_id' => $row['permission_id']])->delete()) {
                throw new \Exception('Could not delete row from permission_role!');
            }
        }

        return true;
    }
}
