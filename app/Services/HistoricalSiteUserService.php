<?php

namespace App\Services;

use App\Models\HistoricalSites;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class HistoricalSiteUserService
{
    public function process(array $data): array
    {
        $result = [
            'status' => 'Not Visited',
            'historicalSiteName' => ''
        ];

        $historicalSiteUser = User::with(['historicalSites' => function($query) use ($data) {
            $query->where('historical_site_id', '=', $data['historical_site_id']);
        }])->where('id', $data['user_id'])->get();

        if (isset($historicalSiteUser[0]?->historicalSites[0]) && $historicalSiteUser[0]?->historicalSites[0]?->historical_site_user) {
            $historicalSiteUser[0]->historicalSites[0]->historical_site_user->update(['is_visited' => $data['is_visited']]);
        } else {
            $query = 'INSERT INTO historical_site_user (historical_site_id, user_id, is_visited, created_at, updated_at) VALUES (?, ?, ?, ?, ?)';
            $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');

            if (! DB::insert($query, [$data['historical_site_id'], $data['user_id'], $data['is_visited'],  $dateTimeNow, $dateTimeNow])) {
                throw new \Exception('Could not insert row in historical_site_user!');
            }
        }

        if ($data['is_visited'] == '1') {
            $result['status'] = 'Visited';
        }

        $historicalSite = HistoricalSites::where('id', $data['historical_site_id'])->first();
        $result['historicalSiteName'] = $historicalSite->name;

        return $result;
    }
}
