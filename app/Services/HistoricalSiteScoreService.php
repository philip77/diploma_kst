<?php

namespace App\Services;

use App\Models\HistoricalSites;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class HistoricalSiteScoreService
{
    public function process(array $data): array
    {
        $result = [
            'rating' => $data['rating'],
            'historicalSiteName' => ''
        ];

        $historicalSiteScore = User::with(['historicalSitesScore' => function($query) use ($data) {
            $query->where('historical_site_id', '=', $data['historical_site_id']);
        }])->where('id', $data['user_id'])->get();

        if (isset($historicalSiteScore[0]?->historicalSitesScore[0]) && $historicalSiteScore[0]?->historicalSitesScore[0]?->historical_site_score) {
            $historicalSiteScore[0]->historicalSitesScore[0]->historical_site_score->update(['rating' => $data['rating']]);
        } else {
            $query = 'INSERT INTO historical_site_score (historical_site_id, user_id, rating, created_at, updated_at) VALUES (?, ?, ?, ?, ?)';
            $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');

            if (! DB::insert($query, [$data['historical_site_id'], $data['user_id'], $data['rating'],  $dateTimeNow, $dateTimeNow])) {
                throw new \Exception('Could not insert row in historical_site_score!');
            }
        }

        $historicalSite = HistoricalSites::where('id', $data['historical_site_id'])->first();
        $result['historicalSiteName'] = $historicalSite->name;

        return $result;
    }
}
