<?php

namespace App\Services;

use App\Models\Comments;
use App\Models\HistoricalSites;
use App\Models\User;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class HistoricalSiteService
{
    public function getRating(HistoricalSites $historicalSite): array
    {
        $rating = 0;
        $countVoters = 0;
        $sumOfRating = 0;
        $floatRating = 0;
        $yourRating = 0;

        $historicalSiteWithScore = HistoricalSites::with('usersScore')->where('id', $historicalSite->id)->get();

        if (isset($historicalSiteWithScore[0]->usersScore)) {
            foreach ($historicalSiteWithScore[0]->usersScore as $pivotTable) {
                if ($pivotTable?->id == auth()->user()->id) {
                    $yourRating = (int) $pivotTable?->historical_site_score?->rating;
                }

                $sumOfRating += (int) $pivotTable?->historical_site_score?->rating;
                $countVoters++;
            }
        }

        if ($sumOfRating && $countVoters) {
            $number = ($sumOfRating / $countVoters);
            $rating = round($number, 0);
            $floatRating = round($number, 1);
        }

        $ratingData = [
            'rating' => $rating,
            'countVoters' => $countVoters,
            'floatRating' => $floatRating,
            'sumOfRating' => $sumOfRating,
            'yourRating' => $yourRating
        ];

        return $ratingData;
    }

    public function getComments(HistoricalSites $historicalSite): Collection
    {
        return Comments::with(['commentAnswers' => function(HasMany $query){
            $query->where('deleted', '=', 0);
        }], ['commentLikeDislikeData' => function(HasMany $query) {
            $query->where('user_id', '=', auth()->user()->id);
        }], 'user')->where('historical_site_id', $historicalSite->id)->where('deleted', 0)->latest('created_at')->get();
    }

    public function getUsersData(Collection $comments): array
    {
        $usersIds = [];
        $usersIds[] = auth()->user()->id;

        foreach ($comments as $comment) {
            if (! in_array($comment->user->id, $usersIds)) {
                $usersIds[] = $comment->user->id;
            }

            if (count($comment->commentAnswers)) {
                foreach ($comment->commentAnswers as $commentAnswer) {
                    if (! in_array($commentAnswer->user_id, $usersIds)) {
                        $usersIds[] = $commentAnswer->user_id;
                    }
                }
            }
        }

        $usersData = User::with('userInfo')->whereIn('id', $usersIds)->get();
        $usersDataArray = [];
        foreach ($usersData as $userData) {
            $usersDataArray[$userData->id] = $userData;
        }

        return $usersDataArray;
    }

    public function isVisited(HistoricalSites $historicalSite): int
    {
        $isVisited = 0;
        $historicalSiteUser = DB::table('historical_site_user')->where('historical_site_id', $historicalSite->id)->where('user_id', auth()->user()->id)->first();
        if (! empty($historicalSiteUser)) {
            $isVisited = (int) $historicalSiteUser->is_visited;
        }

        return $isVisited;
    }

    public function getVisitedHistoricalSites(): Builder
    {
        $whereClause = [];
        $whereClause[] = ['is_visited', '=', 1];
        $whereClause[] = ['user_id', '=', auth()->user()->id];

        $visitedHistoricalSites = DB::table('historical_site_user')->where($whereClause)->get();
        $historicalSitesIds = [];

        foreach ($visitedHistoricalSites as $visitedHistoricalSite) {
            $historicalSitesIds[] = $visitedHistoricalSite->historical_site_id;
        }

        $whereClause = [];
        $whereClause[] = ['confirmed', '=', 1];

        return HistoricalSites::whereIn('id', $historicalSitesIds)->where($whereClause);
    }
}
