<?php

namespace App\Services;

use App\Enum\CommentType;
use App\Enum\CommentValue;
use App\Models\CommentLikeDislikeData;

class CommentLikeDislikeDataService
{
    public function process(array $data): array
    {
        $likeDislikeData = [
            'likes' => ['sign' => '+', 'value' => 0],
            'dislikes' => ['sign' => '+', 'value' => 0],
        ];

        if (! $data['comment_answer_id']) {
            $data['comment_type'] = CommentType::COMMENT->value;
        } else {
            $data['comment_type'] = CommentType::ANSWER->value;
        }

        $uniqueArray = [];
        $uniqueArray['comment_answer_id'] = $data['comment_answer_id'];
        $uniqueArray['comment_id'] = $data['comment_id'];
        $uniqueArray['user_id'] = $data['user_id'];
        $uniqueArray['comment_type'] = $data['comment_type'];

        $row = CommentLikeDislikeData::with('comment', 'commentAnswer')->where($uniqueArray)->get();

        if (count($row)) {
            if ($row[0]->comment_type == CommentType::COMMENT->value) {
                $table = 'comment';
            } else {
                $table = 'commentAnswer';
            }

            if (($row[0]->comment_value === CommentValue::DISLIKE->value) && ($data['comment_value'] === CommentValue::DISLIKE->value)) {
                $this->updateRow($row[0], CommentValue::DELETED->value);
                $this->decreaseDislikesWithOne($row[0], $table);

                $likeDislikeData['dislikes']['sign'] = '-';
                $likeDislikeData['dislikes']['value'] = 1;
            } elseif (($row[0]->comment_value === CommentValue::LIKE->value) && ($data['comment_value'] === CommentValue::LIKE->value)) {
                $this->updateRow($row[0], CommentValue::DELETED->value);
                $this->decreaseLikesWithOne($row[0], $table);

                $likeDislikeData['likes']['sign'] = '-';
                $likeDislikeData['likes']['value'] = 1;
            } else {
                if ($row[0]->comment_value === CommentValue::DELETED->value && $data['comment_value'] === CommentValue::LIKE->value) {
                    $this->increaseLikesWithOne($row[0], $table);

                    $likeDislikeData['likes']['sign'] = '+';
                    $likeDislikeData['likes']['value'] = 1;
                } elseif ($row[0]->comment_value === CommentValue::DELETED->value && $data['comment_value'] === CommentValue::DISLIKE->value) {
                    $this->increaseDislikesWithOne($row[0], $table);

                    $likeDislikeData['dislikes']['sign'] = '+';
                    $likeDislikeData['dislikes']['value'] = 1;
                } elseif ($row[0]->comment_value === CommentValue::DISLIKE->value && $data['comment_value'] === CommentValue::LIKE->value) {
                    $this->decreaseDislikesWithOne($row[0], $table);
                    $this->increaseLikesWithOne($row[0], $table);

                    $likeDislikeData['dislikes']['sign'] = '-';
                    $likeDislikeData['dislikes']['value'] = 1;
                    $likeDislikeData['likes']['sign'] = '+';
                    $likeDislikeData['likes']['value'] = 1;
                } elseif ($row[0]->comment_value === CommentValue::LIKE->value && $data['comment_value'] === CommentValue::DISLIKE->value) {
                    $this->increaseDislikesWithOne($row[0], $table);
                    $this->decreaseLikesWithOne($row[0], $table);

                    $likeDislikeData['dislikes']['sign'] = '+';
                    $likeDislikeData['dislikes']['value'] = 1;
                    $likeDislikeData['likes']['sign'] = '-';
                    $likeDislikeData['likes']['value'] = 1;
                }

                $this->updateRow($row[0], $data['comment_value']);
            }
        } else {
            $commentsCommentLikeDislike = CommentLikeDislikeData::create($data);

            if ($data['comment_type'] == CommentType::COMMENT->value) {
                if ($data['comment_value'] == CommentValue::LIKE->value) {
                    $this->increaseLikesWithOne($commentsCommentLikeDislike, 'comment');

                    $likeDislikeData['likes']['sign'] = '+';
                    $likeDislikeData['likes']['value'] = 1;
                } elseif ($data['comment_value'] == CommentValue::DISLIKE->value) {
                    $this->increaseDislikesWithOne($commentsCommentLikeDislike, 'comment');

                    $likeDislikeData['dislikes']['sign'] = '+';
                    $likeDislikeData['dislikes']['value'] = 1;
                }
            } elseif ($data['comment_type'] == CommentType::ANSWER->value) {
                if ($data['comment_value'] == CommentValue::LIKE->value) {
                    $this->increaseLikesWithOne($commentsCommentLikeDislike, 'commentAnswer');

                    $likeDislikeData['likes']['sign'] = '+';
                    $likeDislikeData['likes']['value'] = 1;
                } elseif ($data['comment_value'] == CommentValue::DISLIKE->value) {
                    $this->increaseDislikesWithOne($commentsCommentLikeDislike, 'commentAnswer');

                    $likeDislikeData['dislikes']['sign'] = '+';
                    $likeDislikeData['dislikes']['value'] = 1;
                }
            }
        }

        return $likeDislikeData;
    }

    private function updateRow(CommentLikeDislikeData $row, string $commentValue): void
    {
        $row->update(['comment_value' => $commentValue]);
    }

    private function increaseLikesWithOne(CommentLikeDislikeData $row, string $table): void
    {
        $likes = $row->$table->likes;
        $likes += 1;
        $row->$table->update(['likes' => $likes]);
    }

    private function increaseDislikesWithOne(CommentLikeDislikeData $row, string $table): void
    {
        $dislikes = $row->$table->dislikes;
        $dislikes += 1;
        $row->$table->update(['dislikes' => $dislikes]);
    }

    private function decreaseLikesWithOne(CommentLikeDislikeData $row, string $table): void
    {
        $likes = $row->$table->likes;
        $likes -= 1;
        $likes = max(0, $likes);
        $row->$table->update(['likes' => $likes]);
    }

    private function decreaseDislikesWithOne(CommentLikeDislikeData $row, string $table): void
    {
        $dislikes = $row->$table->likes;
        $dislikes -= 1;
        $dislikes = max(0, $dislikes);
        $row->$table->update(['dislikes' => $dislikes]);
    }
}
