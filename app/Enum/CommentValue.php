<?php

namespace App\Enum;

enum CommentValue: string
{
    case LIKE = 'like';
    case DISLIKE = 'dislike';
    case DELETED = 'deleted';
}
