<?php

namespace App\Enum;

enum CommentType: string
{
    case COMMENT = 'comment';
    case ANSWER = 'answer';
}
