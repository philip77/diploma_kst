<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\Conversions\Conversion;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\FileAdder;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserInfo extends Model implements HasMedia
{
    use HasFactory; use InteractsWithMedia;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(135)
            ->height(135)
            ->sharpen(10);
    }

    public function getProfilePictureUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        return $this->hasMedia($collectionName) ? $this->getFirstMediaUrl($collectionName, $conversionName) : Storage::url('default/profilePicture.jpeg');
    }

    public function setBirthDateAttribute(?string $value)
    {
        $this->attributes['birth_date'] = $value ? Carbon::createFromFormat('m/d/Y', $value)->format('Y-m-d') : null;
    }

    public function getBirthDateAttribute(): string
    {
        return $this->attributes['birth_date'] ? Carbon::createFromFormat('Y-m-d', $this->attributes['birth_date'])->format('m/d/Y') : '';
    }
}
