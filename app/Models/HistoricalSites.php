<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class HistoricalSites extends Model implements HasMedia
{
    use HasFactory; use InteractsWithMedia;

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class, 'historical_site_user', 'historical_site_id', 'user_id')
            ->withTimestamps()
            ->as('historical_site_user');
    }

    public function usersScore()
    {
        return $this->belongsToMany(User::class, 'historical_site_score', 'historical_site_id', 'user_id')
            ->withTimestamps()
            ->withPivot(['rating'])
            ->as('historical_site_score');
    }

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(513)
            ->height(344)
            ->sharpen(10);
    }

    public function getHistoricalSitePictureUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        return $this->hasMedia($collectionName) ? $this->getFirstMediaUrl($collectionName, $conversionName) : Storage::url('default/historicalSite.jpg');
    }
}
