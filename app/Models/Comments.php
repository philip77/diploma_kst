<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function historicalSite()
    {
        return $this->belongsTo(HistoricalSites::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function commentAnswers()
    {
        return $this->hasMany(CommentAnswers::class, 'comment_id');
    }

    public function commentLikeDislikeData()
    {
        return $this->hasMany(CommentLikeDislikeData::class, 'comment_id');
    }
}
