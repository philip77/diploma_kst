<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userInfo()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function historicalSites()
    {
        return $this->belongsToMany(HistoricalSites::class, 'historical_site_user', 'user_id', 'historical_site_id')
            ->withTimestamps()
            ->as('historical_site_user');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id')
            ->withTimestamps()
            ->as('role_user');
    }

    public function historicalSitesScore()
    {
        return $this->belongsToMany(HistoricalSites::class, 'historical_site_score', 'user_id', 'historical_site_id')
            ->withTimestamps()
            ->withPivot(['rating'])
            ->as('historical_site_score');
    }

    public function comments()
    {
        return $this->hasMany(Comments::class);
    }

    public function commentAnswers()
    {
        return $this->hasMany(CommentAnswers::class);
    }

    public function commentLikeDislikeData()
    {
        return $this->hasMany(CommentLikeDislikeData::class, 'user_id');
    }
}
