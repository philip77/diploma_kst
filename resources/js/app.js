import './bootstrap';
// import 'jquery-ui/external/jquery-3.6.0/jquery.js' // jquery must be the version which provide by jquery-ui
// import 'jquery-ui/dist/themes/base/jquery-ui.min.css';
// import 'jquery-ui/dist/jquery-ui.min'

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
