<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Visited Historical Sites') }}
        </h2>
    </x-slot>

    <x-search-form-historical-site />

    @if($historicalSites->count())
        <div class="container">
            <x-historical-sites-visited :historicalSites="$historicalSites"/>
        </div>
        <div id="pagination">
            {{ $historicalSites->links() }}
        </div>
    @else
        <p id="no-visited-text">Not Visited Historical Sites yet.</p>
    @endif
    <div id="progressbar-container">
        <p>You have visited {{ $countVisitedHistoricalSites }} from {{ $countHistoricalSites }} historical sites</p>
        <div id="progressbar"></div>
    </div>
</x-app-layout>

<script type="text/javascript">
    $(function () {
        let percent = {{ $percentVisitedHistoricalSites }}
        $( "#progressbar" ).progressbar({
            value: percent
        }).children('.ui-progressbar-value')
        .text(`${percent}%`)
        .css('display', 'block').css('background-color', '#090').css('color', 'white');
    });
</script>
