<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Historical Site') }}
        </h2>
    </x-slot>

    <x-historical-site-show :historicalSite="$historicalSite"/>

    <div id="historical-site-widgets">
        <select id="visited-container" data-historical-site-id="{{ $historicalSite->id }}" data-user-id="{{ auth()->user()->id }}" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full" autofocus>
            <option value="0" {{ $isVisited == 0 ? 'selected' : '' }}>Not Visited</option>
            <option value="1" {{ $isVisited == 1 ? 'selected' : '' }}>Visited</option>
        </select>

        <div class="rate-container">
            <div><span>Rating: <b id="rating-value">{{ $ratingData['floatRating'] }}</b></span> / <span>Voters: <b data-sum-rating="{{ $ratingData['sumOfRating'] }}" id="voters-counter">{{ $ratingData['countVoters'] }}</b></span></div>
            <div class="rate">
                <input type="radio" id="star5" name="rate" value="5" {{ $ratingData['rating'] == 5 ? 'checked' : '' }} data-historical-site-id="{{ $historicalSite->id }}" data-user-id="{{ auth()->user()->id }}" />
                <label for="star5" title="Please leave your rating">5 stars</label>
                <input type="radio" id="star4" name="rate" value="4" {{ $ratingData['rating'] == 4 ? 'checked' : '' }} data-historical-site-id="{{ $historicalSite->id }}" data-user-id="{{ auth()->user()->id }}"/>
                <label for="star4" title="Please leave your rating">4 stars</label>
                <input type="radio" id="star3" name="rate" value="3" {{ $ratingData['rating'] == 3 ? 'checked' : '' }} data-historical-site-id="{{ $historicalSite->id }}" data-user-id="{{ auth()->user()->id }}" />
                <label for="star3" title="Please leave your rating">3 stars</label>
                <input type="radio" id="star2" name="rate" value="2" {{ $ratingData['rating'] == 2 ? 'checked' : '' }} data-historical-site-id="{{ $historicalSite->id }}" data-user-id="{{ auth()->user()->id }}"/>
                <label for="star2" title="Please leave your rating">2 stars</label>
                <input type="radio" id="star1" name="rate" value="1" {{ $ratingData['rating'] == 1 ? 'checked' : '' }} data-historical-site-id="{{ $historicalSite->id }}" data-user-id="{{ auth()->user()->id }}"/>
                <label for="star1" title="Please leave your rating">1 star</label>
            </div>
            <div><span>Your Rating: <b id="your-rating">{{ $ratingData['yourRating'] }}</b></span></div>
        </div>
    </div>

    <div id="comments-container">
        <div class="header">
            <i class="fa fa-angle-left"></i>
            <span class="title">Comments</span>
            <i class="fa fa-paper-plane"></i>
        </div>

        <div class="comments-form">
            <form id="form-comment">
                <input type="hidden" name="historical_site_id" id="historical_site_id" value="{{ $historicalSite->id }}">
                <header>
                    <img src="{{ $usersData[auth()->user()->id]->userInfo->getProfilePictureUrl('profileImage', 'thumb') }}">
                    <h2>Want to participate?</h2>
                </header>

                <div class="content">
                    <textarea name="comment" id="comment" class="comment-body" rows="5" placeholder="Quick, thing of something to say!" required></textarea>
                    <x-input-error class="mt-2" :messages="$errors->get('comment')" />
                </div>
                <p class="text-red-500 text-xs mt-2 error-text" hidden></p>

                <div>
                    <div class="flex items-center gap-4">
                        <x-primary-button>{{ __('Save') }}</x-primary-button>
                    </div>
                </div>
            </form>
        </div>

        {{--Comment Section--}}

        <div class="body">
            @foreach($comments as $comment)
                <div class="section" id="section-{{ $comment->id }}">
                    {{--                Main comment--}}
                    <div>
                        <div class="img">
                            <img src="{{ $usersData[$comment->user->id]->userInfo->getProfilePictureUrl('profileImage', 'thumb') }}" alt="">
                        </div>

                        <div class="comment-body">
                            <span class="name">{{ $comment->user->username }}</span>
                            <span>{{ $comment->comment }}</span>

                            <br>

                            <div class="muted">
                                <span>{{ $comment->created_at->format('Y-m-d') }}</span>
                                <span><b><span id="likes-numbers-comment-{{ $comment->id }}">{{ $comment->likes }}</span> Like</b></span>
                                <span><b><span id="dislikes-numbers-comment-{{ $comment->id }}">{{ $comment->dislikes }}</span> Dislike</b></span>
                                @if (auth()->user()->id == $comment->user_id)
                                    <span class="delete-comment" data-delete-comment-id="{{ $comment->id }}"><i class="fa fa-trash"></i></span>
                                @endif
                                <span class="replay-button" data-comment-id="{{ $comment->id }}"><b>Reply</b></span>
                            </div>

                            <form class="form-comment-replay" data-answer-for-comment="{{ $comment->id }}" hidden>
                                <div class="content">
                                    <x-text-input id="comment-answer-{{ $comment->id }}" name="comment_answer" type="text" class="mt-1 block w-full" :value="old('comment_answer')" autofocus />
                                </div>
                                <p class="text-red-500 text-xs mt-2 error-text" id="error-text-{{ $comment->id }}" hidden></p>

                                <div>
                                    <div class="flex items-center gap-4">
                                        <x-primary-button>{{ __('Save') }}</x-primary-button>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="like-dislike">
                            @php
                                $classThumbUp = 'fa fa-thumbs-o-up';
                                $classThumbDown = 'fa fa-thumbs-o-down';

                                foreach ($comment->commentLikeDislikeData as $row) {
                                    if ($row->comment_type == \App\Enum\CommentType::COMMENT->value && $row->comment_value == \App\Enum\CommentValue::LIKE->value && $row->user_id == auth()->user()->id) {
                                        $classThumbUp = 'fa fa-thumbs-up';
                                    } elseif ($row->comment_type == \App\Enum\CommentType::COMMENT->value && $row->comment_value == \App\Enum\CommentValue::DISLIKE->value && $row->user_id == auth()->user()->id) {
                                        $classThumbDown = 'fa fa-thumbs-down';
                                    }
                                }
                            @endphp
                            <span class="comment-like-dislike" data-comment-id="{{$comment->id}}" data-comment-answer-id="" data-value="like" data-user-id="{{ auth()->user()->id }}"><i class="{{ $classThumbUp }}"></i></span>
                            <span class="comment-like-dislike" data-comment-id="{{$comment->id}}" data-comment-answer-id="" data-value="dislike" data-user-id="{{ auth()->user()->id }}"><i class="{{ $classThumbDown }}"></i></span>
                        </div>
                    </div>
                    {{--                Replay --}}
                    @if (count($comment->commentAnswers))
                        @foreach($comment->commentAnswers as $commentAnswer)
                            <div class="replay" id="answer-{{ $commentAnswer->id }}">
                                <div>
                                    <div class="img">
                                        <img src="{{ $usersData[$commentAnswer->user_id]->userInfo->getProfilePictureUrl('profileImage', 'thumb') }}" alt="">
                                    </div>

                                    <div class="comment-body">
                                        <span class="name">{{ $usersData[$commentAnswer->user_id]->username }}</span>
                                        <span>{{ $commentAnswer->answer }}</span>

                                        <br>

                                        <div class="muted">
                                            <span>{{ $commentAnswer->created_at->format('Y-m-d') }}</span>
                                            <span><b><span id="likes-numbers-answer-{{ $commentAnswer->id }}">{{ $commentAnswer->likes }}</span> Like</b></span>
                                            <span><b><span id="dislikes-numbers-answer-{{ $commentAnswer->id }}">{{ $commentAnswer->dislikes }}</span> Dislike</b></span>
                                            @if (auth()->user()->id == $commentAnswer->user_id)
                                                <span class="delete-comment-answer" data-delete-comment-answer-id="{{ $commentAnswer->id }}"><i class="fa fa-trash"></i></span>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="like-dislike">
                                        @php
                                            $classThumbUp = 'fa fa-thumbs-o-up';
                                            $classThumbDown = 'fa fa-thumbs-o-down';

                                            foreach ($comment->commentLikeDislikeData as $row) {
                                                if ($row->comment_type == \App\Enum\CommentType::ANSWER->value && $row->comment_answer_id == $commentAnswer->id && $row->comment_value == \App\Enum\CommentValue::LIKE->value && $row->user_id == auth()->user()->id) {
                                                    $classThumbUp = 'fa fa-thumbs-up';
                                                } elseif ($row->comment_type == \App\Enum\CommentType::ANSWER->value && $row->comment_answer_id == $commentAnswer->id && $row->comment_value == \App\Enum\CommentValue::DISLIKE->value && $row->user_id == auth()->user()->id) {
                                                    $classThumbDown = 'fa fa-thumbs-down';
                                                }
                                            }
                                        @endphp
                                        <span class="comment-like-dislike" data-comment-id="{{$comment->id}}" data-comment-answer-id="{{ $commentAnswer->id }}" data-value="like" data-user-id="{{ auth()->user()->id }}"><i class="{{ $classThumbUp }}"></i></span>
                                        <span class="comment-like-dislike" data-comment-id="{{$comment->id}}" data-comment-answer-id="{{ $commentAnswer->id }}" data-value="dislike" data-user-id="{{ auth()->user()->id }}"><i class="{{ $classThumbDown }}"></i></span>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            @endforeach
        </div>

        <div id="dialog-confirm-comment" title="Delete Comment!" hidden>
            <p>These comment will be deleted. Are you sure?</p>
        </div>

        <div id="dialog-confirm-comment-answer" title="Delete Answer For Comment!" hidden>
            <p>These answer will be deleted. Are you sure?</p>
        </div>
    </div>
</x-app-layout>

<script type="text/javascript">
    $(function (){
        let commentsContainer = $('#comments-container');

        commentsContainer.on('click', '.delete-comment', function () {
            let id = $(this).data('delete-comment-id');

            $('#dialog-confirm-comment').dialog({
                resizable: false,
                height: 'auto',
                width: 400,
                modal: true,
                buttons: {
                    'Delete comment': function() {
                        $.ajax({
                            url: "/destroy-comment",
                            type: 'POST',
                            data: {
                                '_token': "{{ csrf_token() }}",
                                id
                            },
                            success: function(response) {
                                $(`#section-${id}`).hide();
                            },
                            error: function(response) {

                            },
                        });

                        $(this).dialog('close');
                    },
                    Cancel: function() {
                        $(this).dialog('close');
                    }
                }
            });
        });

        commentsContainer.on('click', '.delete-comment-answer', function () {
            let id = $(this).data('delete-comment-answer-id');

            $('#dialog-confirm-comment-answer').dialog({
                resizable: false,
                height: 'auto',
                width: 400,
                modal: true,
                buttons: {
                    'Delete answer': function() {
                        $.ajax({
                            url: "/destroy-comment-answer",
                            type: 'POST',
                            data: {
                                '_token': "{{ csrf_token() }}",
                                id
                            },
                            success: function(response) {
                                $(`#answer-${id}`).hide();
                            },
                            error: function(response) {

                            },
                        });

                        $(this).dialog('close');
                    },
                    Cancel: function() {
                        $(this).dialog('close');
                    }
                }
            });
        });

        commentsContainer.on('click', '.replay-button', function (){
            let commentId = $(this).attr('data-comment-id');
            $(`[data-answer-for-comment="${commentId}"]`).toggle();
        });

        commentsContainer.on('submit', '.form-comment-replay', function (e) {
            e.preventDefault();
            let comment_id = $(this).attr('data-answer-for-comment');
            let answerElement = $(`#comment-answer-${comment_id}`);
            let answer = answerElement.val();
            let errorTextEle = ($(`#error-text-${comment_id}`));
            let userId = {{ auth()->user()->id }}

            $.ajax({
                url: "/store-comment-answer",
                type: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    comment_id,
                    answer
                },
                success: function(response) {
                    let date = new Date();
                    let dateFormat = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

                    answerElement.val('');
                    $(`[data-answer-for-comment="${comment_id}"]`).hide();

                    $(`#section-${comment_id}`).append(
                        $(`<div class="replay" id="answer-${response.commentAnswer.id}"></div>`).append(
                            $('<div></div>').append(
                                $('<div class="img"></div>').append($('<img src={{ $usersData[auth()->user()->id]->userInfo->getProfilePictureUrl('profileImage', 'thumb') }}>'))
                            ).append(
                                $('<div class="comment-body"></div>').append(
                                    $('<span class="name">{{ $usersData[auth()->user()->id]->username }}</span>')
                                ).append($('<span></span>').text(answer)).append($('<br>')).append(
                                    $('<div class="muted"></div>').append($(`<span>${dateFormat}</span>`)).append($(`<span><b><span id="likes-numbers-answer-${response.commentAnswer.id}">${response.commentAnswer.likes}</span> Like</b></span>`)).append($(`<span><b><span id="dislikes-numbers-answer-${response.commentAnswer.id}">${response.commentAnswer.dislikes}</span> Dislike</b></span>`)).append($(`<span class="delete-comment-answer" data-delete-comment-answer-id="${response.commentAnswer.id}"><i class="fa fa-trash"></i></span>`))
                                )
                            ).append($('<div class="like-dislike"></div>').append($(`<span class="comment-like-dislike" data-comment-id="${comment_id}" data-comment-answer-id="${response.commentAnswer.id}" data-value="like" data-user-id="${userId}"></span>`).append($('<i class="fa fa-thumbs-o-up"></i>'))).append($(`<span class="comment-like-dislike" data-comment-id="${comment_id}" data-comment-answer-id="${response.commentAnswer.id}" data-value="dislike" data-user-id="${userId}"></span>`).append($('<i class="fa fa-thumbs-o-down"></i>'))))
                        )
                    );
                },
                error: function(response) {
                    errorTextEle.text(response.responseJSON.message);
                    errorTextEle.show();
                    setTimeout(() => {
                        errorTextEle.hide();
                    }, 5000);
                },
            });
        });

        commentsContainer.on('submit', '#form-comment', function (e){
            e.preventDefault();
            let historical_site_id = $('#historical_site_id').val();
            let comment = $('#comment').val();
            let userId = {{ auth()->user()->id }};

            $.ajax({
                url: "/store-comment",
                type: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    historical_site_id,
                    comment
                },
                success: function(response){
                    let date = new Date();
                    let dateFormat = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);

                    $('#comment').val('');

                    $('.body').prepend(
                        $(`<div class="section" id="section-${response.comment.id}"></div>`).append(
                            $('<div></div>').append(
                                $('<div class="img"></div>').append($('<img src={{ $usersData[auth()->user()->id]->userInfo->getProfilePictureUrl('profileImage', 'thumb') }}>'))
                            ).append(
                                $('<div class="comment-body"></div>').append(
                                    $('<span class="name">{{ $usersData[auth()->user()->id]->username }}</span>')
                                ).append($('<span></span>').text(comment)).append($('<br>')).append(
                                    $('<div class="muted"></div>').append($(`<span>${dateFormat}</span>`)).append($(`<span><b><span id="likes-numbers-comment-${response.comment.id}">${response.comment.likes}</span> Like</b></span>`)).append($(`<span><b><span id="dislikes-numbers-comment-${response.comment.id}">${response.comment.dislikes}</span> Dislike</b></span>`)).append($(`<span class="delete-comment" data-delete-comment-id="${response.comment.id}"><i class="fa fa-trash"></i></span>`)).append($(`<span class="replay-button" data-comment-id="${response.comment.id}"><b>Reply</b></span>`))
                                ).append($(`<form class="form-comment-replay" data-answer-for-comment="${response.comment.id}" hidden></form>`)
                                    .append($('<div class="content"></div>').append(`<x-text-input id="comment-answer-${response.comment.id}" name="comment_answer" type="text" class="mt-1 block w-full" autofocus/>`))
                                    .append(`<p class="text-red-500 text-xs mt-2 error-text" id="error-text-${response.comment.id}" hidden></p>`)
                                    .append($('<div></div>').append($('<div class="flex items-center gap-4"></div>').append('<button type="submit" class="inline-flex items-center px-4 py-2 bg-gray-800 dark:bg-gray-200 border border-transparent rounded-md font-semibold text-xs text-white dark:text-gray-800 uppercase tracking-widest hover:bg-gray-700 dark:hover:bg-white focus:bg-gray-700 dark:focus:bg-white active:bg-gray-900 dark:active:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:focus:ring-offset-gray-800 transition ease-in-out duration-150">Save</button>')))
                                )
                            ).append($('<div class="like-dislike"></div>').append($(`<span class="comment-like-dislike" data-comment-id="${response.comment.id}" data-comment-answer-id="" data-value="like" data-user-id="${userId}"></span>`).append($('<i class="fa fa-thumbs-o-up"></i>'))).append($(`<span class="comment-like-dislike" data-comment-id="${response.comment.id}" data-comment-answer-id="" data-value="dislike" data-user-id="${userId}"></span>`).append($('<i class="fa fa-thumbs-o-down"></i>'))))
                        )
                    );
                },
                error: function(response) {
                    let errorTextEle = $('.error-text');
                    errorTextEle.text(response.responseJSON.message);
                    errorTextEle.show();
                    setTimeout(() => {
                        errorTextEle.hide();
                    }, 5000);
                },
            });
        });

        commentsContainer.on('click', '.comment-like-dislike', function () {
            let comment_id = $(this).data('comment-id');
            let comment_answer_id = $(this).data('comment-answer-id');
            let comment_value = $(this).data('value');
            let user_id = $(this).data('user-id');
            let thumbElement = $(this).children();
            let thumbClass = thumbElement.attr('class');

            let parent = $(this).parent();
            let firstChild = parent.find($('span:first-child')).children();
            let secondChild = parent.find($('span:nth-child(2)')).children();

            $.ajax({
                url: "/store-comment-like",
                type: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    comment_id,
                    comment_answer_id,
                    comment_value,
                    user_id
                },
                success: function(response){
                    let likeDislikeData = response[0].likeDislikeData;
                    let likesData = likeDislikeData.likes;
                    let dislikesData = likeDislikeData.dislikes;

                    let thumbOptions = {
                        'fa fa-thumbs-o-up': 'fa fa-thumbs-up',
                        'fa fa-thumbs-up': 'fa fa-thumbs-o-up',
                        'fa fa-thumbs-o-down': 'fa fa-thumbs-down',
                        'fa fa-thumbs-down': 'fa fa-thumbs-o-down'
                    }

                    if (thumbOptions.hasOwnProperty(thumbClass)) {
                        let newClass = thumbOptions[thumbClass];

                        if (newClass) {
                            firstChild.removeClass().addClass('fa fa-thumbs-o-up');
                            secondChild.removeClass().addClass('fa fa-thumbs-o-down');
                            thumbElement.removeClass().addClass(newClass);

                            let likeSelector = '';
                            let dislikeSelector = '';

                            if (comment_answer_id) {
                                likeSelector = `#likes-numbers-answer-${comment_answer_id}`;
                                dislikeSelector = `#dislikes-numbers-answer-${comment_answer_id}`;
                            } else {
                                likeSelector = `#likes-numbers-comment-${comment_id}`;
                                dislikeSelector = `#dislikes-numbers-comment-${comment_id}`;
                            }

                            // likes Data
                            let likesValue = parseInt($(likeSelector).text());
                            if (likesData.sign === '+') {
                                likesValue += likesData.value;
                            } else {
                                likesValue -= likesData.value;
                            }

                            let dislikesValue = parseInt($(dislikeSelector).text());
                            if (dislikesData.sign === '+') {
                                dislikesValue += dislikesData.value;
                            } else {
                                dislikesValue -= dislikesData.value;
                            }

                            $(likeSelector).text(likesValue);
                            $(dislikeSelector).text(dislikesValue);
                        }
                    }
                },
                error: function(response) {
                },
            });
        });

        $('#historical-site-widgets').on('change', '#visited-container', function () {
            let historical_site_id = $(this).data('historical-site-id');
            let user_id = $(this).data('user-id');
            let is_visited = $(this).val();

            $.ajax({
                url: "/visited-historical-site",
                type: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    historical_site_id,
                    user_id,
                    is_visited
                },
                success: function(response){
                    let successMessage = $('<div id="success-message"></div>');
                    $('body > div > main').prepend(successMessage.append($('<p></p>').text(`You set "${response[0].historicalSiteData.historicalSiteName}" to status "${response[0].historicalSiteData.status}"!`)));

                    setTimeout(function (){
                        $('#success-message').remove();
                    }, 4000);
                },
                error: function(response) {
                },
            });
        });

        $('.rate').on('change', 'input[name="rate"]', function (e) {
            let historical_site_id = $(this).data('historical-site-id');
            let user_id = $(this).data('user-id');
            let rating = $(this).val();

            $.ajax({
                url: "/rating-historical-site",
                type: 'POST',
                data: {
                    '_token': "{{ csrf_token() }}",
                    historical_site_id,
                    user_id,
                    rating
                },
                success: function(response){
                    let newRating = parseInt(response[0].historicalSiteScoreData.rating);
                    let votersCounter = $('#voters-counter');
                    let votersCounterValue = parseInt(votersCounter.text());
                    let sumRating = parseInt(votersCounter.data('sum-rating'));
                    let yourRating = $('#your-rating');
                    let yourRatingValue = parseInt(yourRating.text());

                    if (yourRatingValue) {
                        votersCounterValue--;
                        sumRating -= yourRatingValue;
                    }

                    votersCounterValue++;
                    sumRating += newRating;

                    let number = sumRating / votersCounterValue;
                    let rating = Math.round(number)
                    let ratingFloat = (Math.round(number * 10) / 10);

                    $(`#star${rating}`).prop('checked', true);
                    yourRating.text(newRating);
                    votersCounter.text(votersCounterValue);
                    votersCounter.data('sum-rating', sumRating);
                    $('#rating-value').text(ratingFloat);
                },
                error: function(response) {
                },
            });
        });
    });
</script>
