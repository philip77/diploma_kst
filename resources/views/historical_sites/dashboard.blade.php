<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <x-search-form-historical-site />

    @if($historicalSites->count())
        <div class="container">
            <x-historical-sites :historicalSites="$historicalSites"/>
        </div>
        <div id="pagination">
            {{ $historicalSites->links() }}
        </div>
    @else
        <p>No Historical Sites yet. Please check back later.</p>
    @endif
</x-app-layout>
