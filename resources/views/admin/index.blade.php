<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Index') }}
        </h2>
    </x-slot>

    <div id="admin-button-menu">
        <a id="app-button" href="{{ route('admin.show.users') }}" class="button">Show All Users</a>
        <a id="app-button" href="{{ route('admin.create.role') }}" class="button">Create Role</a>
        <a id="app-button" href="{{ route('admin.show.historical.sites') }}" class="button">Historical Sites</a>
    </div>
</x-app-layout>
