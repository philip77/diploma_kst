<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Show Historical Sites') }}
        </h2>
    </x-slot>
    <x-search-form-historical-site />
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Historical Site Name</th>
            <th>Historical Site Info</th>
            <th>Is Confirmed Historical Site</th>
            <th>Show Historical Site</th>
        </tr>
        </thead>
        <tbody>
        @foreach($historicalSites as $historicalSite)
            <tr>
                <td data-label="Id">{{ $historicalSite->id }}</td>
                <td data-label="Historical Site Name">{{ $historicalSite->name }}</td>
                <td data-label="Historical Site Info">{{ Str::limit($historicalSite->data, 50, $end = '...') }}</td>
                <td data-label="Is Confirmed Historical Site">{{ $historicalSite->confirmed ? 'Yes' : 'No' }}</td>
                <td data-label="Show Historical Site">
                    <a id="table-button" href="{{ route('admin.show.historical.site', [$historicalSite->id]) }}" >Show Historical Site</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="pagination">
        {{ $historicalSites->links() }}
    </div>

    <div id="button-container">
        <a id="app-button" href="{{ route('admin.index') }}" class="button">Back</a>
        <a id="app-button" href="{{ route('admin.create.historical.site') }}" class="button">Add</a>
    </div>
</x-app-layout>
