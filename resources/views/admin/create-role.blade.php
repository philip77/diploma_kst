<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Create Role') }}
        </h2>
    </x-slot>

    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Set Permission Role</th>
        </tr>
        </thead>
        <tbody>
        @foreach($roles as $role)
            <tr>
                <td data-label="Id">{{ $role->id }}</td>
                <td data-label="Name">{{ $role->name }}</td>
                <td data-label="Set Permission Role">
                    <a id="table-button" href="{{ route('admin.create.permission.role', [$role->id]) }}" >Set Permission Role</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div id="pagination">
        {{ $roles->links() }}
    </div>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-6">
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <section>
                    <form method="post" action="{{ route('admin.store.role') }}" class="mt-6 space-y-6">
                        @csrf

                        <div>
                            <x-input-label for="role" :value="__('Role')" />
                            <x-text-input id="role" name="name" type="text" class="mt-1 block w-full" :value="old('name')" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('name')" />
                        </div>

                        <div class="flex items-center gap-4">
                            <x-primary-button>{{ __('Save') }}</x-primary-button>

                            @if (session('status') === 'role-store')
                                <p
                                    x-data="{ show: true }"
                                    x-show="show"
                                    x-transition
                                    x-init="setTimeout(() => show = false, 2000)"
                                    class="text-sm text-gray-600 dark:text-gray-400"
                                >{{ __('Saved.') }}</p>
                            @endif
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <div id="button-container">
        <a id="app-button" href="{{ route('admin.index') }}" class="button">Back</a>
    </div>
</x-app-layout>
