<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Show Historical Site') }}
        </h2>
    </x-slot>
    <x-historical-site-admin :historicalSite="$historicalSite"/>
</x-app-layout>
