<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Add Permission to Role') }}
        </h2>
    </x-slot>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-6">
        <a id="app-button" href="{{ route('admin.create.role') }}" class="button">Back</a>
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <section>
                    <h3>{{__('Add permissions for ' . $role->name . ' role!')}}</h3>
                    <form method="post" action="{{ route('admin.update.permission.role', [$role->id]) }}" class="mt-6 space-y-6">
                        @csrf

                        <input type="hidden" name="class" value="{{ $class }}">
                        <div>
                            @foreach($methodsInAdminControllerFiltered as $action)
                                @php
                                    $isChecked = '';
                                    foreach ($role->permissions as $permission) {
                                        if ($permission->controller == $class && $permission->action == $action->name) {
                                            $isChecked = 'checked';
                                        }
                                    }
                                @endphp
                                <x-input-label for="{{ $action->name }}" value="{{ $action->name }}" />
                                <input type="checkbox" value="{{ $action->name }}" id="{{ $action->name }}" {{ $isChecked }} name="actions[]">
                            @endforeach
                            <x-input-error class="mt-2" :messages="$errors->get('actions[]')" />
                        </div>

                        <div class="flex items-center gap-4">
                            <x-primary-button>{{ __('Save') }}</x-primary-button>

                            @if (session('status') === 'role-permission-store')
                                <p
                                    x-data="{ show: true }"
                                    x-show="show"
                                    x-transition
                                    x-init="setTimeout(() => show = false, 2000)"
                                    class="text-sm text-gray-600 dark:text-gray-400"
                                >{{ __('Saved.') }}</p>
                            @endif
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</x-app-layout>
