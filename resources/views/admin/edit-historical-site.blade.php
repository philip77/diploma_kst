<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Add Historical Site') }}
        </h2>
    </x-slot>

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-6">
        <a id="app-button" href="{{ route('admin.show.historical.site', [$historicalSite->id]) }}" class="button">Back</a>
        <div class="p-4 sm:p-8 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
            <div class="max-w-xl">
                <section>
                    <form method="post" enctype="multipart/form-data" action="{{ route('admin.update.historical.site', [$historicalSite->id]) }}" class="mt-6 space-y-6">
                        @csrf
                        @method('patch')

                        <div>
                            <x-input-label for="name" :value="__('Name')" />
                            <x-text-input id="name" name="name" type="text" class="mt-1 block w-full" :value="old('name', $historicalSite->name)" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('name')" />
                        </div>

                        <div>
                            <x-input-label for="data" :value="__('Data')" />
                            <textarea id="data" name="data" class="mt-1 block w-full" autofocus>{{ old('data', $historicalSite->data) }}</textarea>
                            <x-input-error class="mt-2" :messages="$errors->get('data')" />
                        </div>

                        <div>
                            <x-input-label for="confirmed" :value="__('Confirmed')" />
                            <select id="confirmed" name="confirmed" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full" autofocus>
                                <option value="0" {{ old('confirmed', $historicalSite->confirmed) == '0' ? 'selected' : '' }}>Not Confirmed</option>
                                <option value="1" {{ old('confirmed', $historicalSite->confirmed) == '1' ? 'selected' : '' }}>Confirmed</option>
                            </select>
                            <x-input-error class="mt-2" :messages="$errors->get('confirmed')" />
                        </div>

                        <div>
                            <x-input-label for="historical-site-image" :value="__('Historical Site Image')" />
                            <image src="{{ $historicalSite->getHistoricalSitePictureUrl('historicalSiteImage', 'thumb') }}"></image>
                            <x-text-input id="historical-site-image" name="historical_site_image" type="file" class="mt-1 block w-full" autofocus />
                            <x-input-error class="mt-2" :messages="$errors->get('historical_site_image')" />
                        </div>

                        <div class="flex items-center gap-4">
                            <x-primary-button>{{ __('Save') }}</x-primary-button>

                            @if (session('status') === 'historical-site-update')
                                <p
                                    x-data="{ show: true }"
                                    x-show="show"
                                    x-transition
                                    x-init="setTimeout(() => show = false, 2000)"
                                    class="text-sm text-gray-600 dark:text-gray-400"
                                >{{ __('Saved.') }}</p>
                            @endif

                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
</x-app-layout>
