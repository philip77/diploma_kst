<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Admin Panel Show Users') }}
        </h2>
    </x-slot>
    <x-search-form-user />
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>Email</th>
            <th>First Name</th>
            <th>Middle Name</th>
            <th>Last Name</th>
            <th>Set Role</th>
        </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td data-label="Id">{{ $user->id }}</td>
                    <td data-label="Username">{{ $user->username }}</td>
                    <td data-label="Email">{{ $user->email }}</td>
                    <td data-label="First Name">{{ $user->userInfo->first_name }}</td>
                    <td data-label="Middle Name">{{ $user->userInfo->middle_name }}</td>
                    <td data-label="Last Name">{{ $user->userInfo->last_name }}</td>
                    <td data-label="Set Role">
                        <a id="table-button" href="{{ route('admin.set.user.role',[$user->id]) }}" >Set Role</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div id="pagination">
        {{ $users->links() }}
    </div>

    <div id="button-container">
        <a id="app-button" href="{{ route('admin.index') }}" class="button">Back</a>
    </div>
</x-app-layout>
