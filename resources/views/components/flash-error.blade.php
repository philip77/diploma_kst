@if (session()->has('error'))
    <div x-data="{show: true}"
         x-init="setTimeout(() => show = false, 4000)"
         x-show="show"
         id="error-message">
        <p>{{ session()->get('error') }}</p>
    </div>
@endif
