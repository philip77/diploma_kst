<div class="search-form-container">
    <form method="get" class="search-form">
        <div>
            <x-input-label for="search-email" :value="__('Search user by email:')" />
            <x-text-input id="search-email" name="search_email" type="text" class="mt-1 block w-full" value="{{ request('search_email') }}"  />
        </div>
    </form>
</div>
