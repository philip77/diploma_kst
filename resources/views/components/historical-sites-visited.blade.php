@props(['historicalSites'])

@foreach($historicalSites as $historicalSite)
    <section class="card">
        <div class="media">
            <img src="{{ $historicalSite->getHistoricalSitePictureUrl('historicalSiteImage', 'thumb') }}" alt="">
        </div>
        <div class="content">
            <h3>{{ $historicalSite->name }}</h3>
        </div>
    </section>
@endforeach
