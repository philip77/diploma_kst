@props(['historicalSite'])

<div class="page">
    <main>
        <article>
            <header>

                <h1>{{ $historicalSite->name }}</h1>
            </header>
            <section>
                <p>
                    <img src="{{ $historicalSite->getHistoricalSitePictureUrl('historicalSiteImage', 'thumb') }}" alt="" class="align-right">
                    {{ $historicalSite->data }}

                </p>
            </section>

        </article>

    </main>
    <a id="app-button" href="{{ route('admin.show.historical.sites') }}" class="button">Back</a>
    <a id="app-button" href="{{ route('admin.edit.historical.site', [$historicalSite->id]) }}" class="button">Edit</a>
</div>
