<div class="search-form-container">
    <form method="get" class="search-form">
        <div>
            <x-input-label for="search-name" :value="__('Search historical site by name:')" />
            <x-text-input id="search-name" name="search_name" type="text" class="mt-1 block w-full" value="{{ request('search_name') }}"  />
        </div>
    </form>
</div>
