@props(['historicalSites'])

@foreach($historicalSites as $historicalSite)
    <section class="card">
        <div class="media">
            <img src="{{ $historicalSite->getHistoricalSitePictureUrl('historicalSiteImage', 'thumb') }}" alt="">
        </div>
        <div class="content">
            <h3>{{ $historicalSite->name }}</h3>
            <p>{{ Str::limit($historicalSite->data, 50, $end = '...') }}</p>
            <a href="/historical-site-show/{{ $historicalSite->id }}" class="button">More information</a>
        </div>
    </section>
@endforeach
