@props(['historicalSite'])

<div class="page">
    <header>
        <p class="site-title">Historical Site</p>
    </header>
    <article>
        <header>
            <a id="app-button" href="{{ route('historical-sites.index') }}" class="button">Back</a>
            <h1>{{ $historicalSite->name }}</h1>
        </header>
        <section>
            <p>
                <img src="{{ $historicalSite->getHistoricalSitePictureUrl('historicalSiteImage', 'thumb') }}" alt="" class="align-right">
                {{ $historicalSite->data }}
            </p>
        </section>
    </article>
</div>

