@if (session()->has('success'))
    <div x-data="{show: true}"
         x-init="setTimeout(() => show = false, 4000)"
         x-show="show"
         id="success-message">
        <p>{{ session()->get('success') }}</p>
    </div>
@endif
