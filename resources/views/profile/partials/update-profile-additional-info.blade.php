<section>
    <header>
        <h2 class="text-lg font-medium text-gray-900 dark:text-gray-100">
            {{ __('Profile Additional Information') }}
        </h2>

        <p class="mt-1 text-sm text-gray-600 dark:text-gray-400">
            {{ __("Update your account's profile additional information.") }}
        </p>
    </header>

    <form id="send-verification" method="post" action="{{ route('verification.send') }}">
        @csrf
    </form>

    <form method="post" enctype="multipart/form-data" action="{{ route('profile.info.update', ['userInfo' => $userInfo->id]) }}" class="mt-6 space-y-6">
        @csrf
        @method('patch')

        <div>
            <x-input-label for="first-name" :value="__('First Name')" />
            <x-text-input id="first-name" name="first_name" type="text" class="mt-1 block w-full" :value="old('first_name', $userInfo->first_name)" autofocus />
            <x-input-error class="mt-2" :messages="$errors->get('first_name')" />
        </div>

        <div>
            <x-input-label for="middle-name" :value="__('Middle Name')" />
            <x-text-input id="middle-name" name="middle_name" type="text" class="mt-1 block w-full" :value="old('middle_name', $userInfo->middle_name)" autofocus />
            <x-input-error class="mt-2" :messages="$errors->get('middle_name')" />
        </div>

        <div>
            <x-input-label for="last-name" :value="__('Last Name')" />
            <x-text-input id="last-name" name="last_name" type="text" class="mt-1 block w-full" :value="old('last_name', $userInfo->last_name)" autofocus />
            <x-input-error class="mt-2" :messages="$errors->get('last_name')" />
        </div>

        <div>
            <x-input-label for="gender" :value="__('Gender')" />

            <select id="gender" name="gender" class="border-gray-300 dark:border-gray-700 dark:bg-gray-900 dark:text-gray-300 focus:border-indigo-500 dark:focus:border-indigo-600 focus:ring-indigo-500 dark:focus:ring-indigo-600 rounded-md shadow-sm mt-1 block w-full" autofocus>
                <option value=""></option>
                @foreach($genderOptions as $genderOption)
                    <option value="{{ $genderOption }}" {{ old('gender', $userInfo->gender) == $genderOption ? 'selected' : '' }} >{{ $genderOption }}</option>
                @endforeach
            </select>
            <x-input-error class="mt-2" :messages="$errors->get('gender')" />
        </div>

        <div>
            <x-input-label for="datepicker" :value="__('Date Of Birth')" />
            <x-text-input id="datepicker" name="birth_date" type="text" class="mt-1 block w-full" :value="old('birth_date', $userInfo->birth_date)" autofocus />
            <x-input-error class="mt-2" :messages="$errors->get('birth_date')" />
        </div>

        <div>
            <x-input-label for="profile-image" :value="__('Profile Image')" />
            <image src="{{ $userInfo->getProfilePictureUrl('profileImage', 'thumb') }}"></image>
            <x-text-input id="profile-image" name="profile_image" type="file" class="mt-1 block w-full" autofocus />
            <x-input-error class="mt-2" :messages="$errors->get('profile_image')" />
        </div>

        <div class="flex items-center gap-4">
            <x-primary-button>{{ __('Save') }}</x-primary-button>

            @if (session('status') === 'profile-updated')
                <p
                    x-data="{ show: true }"
                    x-show="show"
                    x-transition
                    x-init="setTimeout(() => show = false, 2000)"
                    class="text-sm text-gray-600 dark:text-gray-400"
                >{{ __('Saved.') }}</p>
            @endif
        </div>
    </form>
</section>

<script>
    $(function() {
        $("#datepicker").datepicker({
            minDate: '-120Y',
            maxDate: '0',
            changeMonth: true,
            changeYear: true
        });
    });
</script>
