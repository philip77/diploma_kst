<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfileInfoController;
use App\Http\Controllers\HistoricalSitesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');


    Route::patch('/profile-info/{userInfo}', [ProfileInfoController::class, 'update'])->name('profile.info.update');

    Route::get('/dashboard', [HistoricalSitesController::class, 'dashboard'])->name('dashboard');
    Route::get('/historical-site-show/{historicalSite}', [HistoricalSitesController::class, 'historicalSite'])->name('historical.site.show');
});

Route::get('/', [HistoricalSitesController::class, 'index'])->name('historical-sites.index');
Route::get('/historical-site/{historicalSite}', [HistoricalSitesController::class, 'show'])->name('historical.site.show');

Route::post('/store-comment', [\App\Http\Controllers\Comment::class, 'storeComment'])->name('store.comment')->middleware('auth');
Route::post('/destroy-comment', [\App\Http\Controllers\Comment::class, 'destroyComment'])->name('destroy.comment')->middleware('auth');

Route::post('/store-comment-answer', [\App\Http\Controllers\CommentAnswer::class, 'storeCommentAnswer'])->name('store.comment.answer')->middleware('auth');
Route::post('/destroy-comment-answer', [\App\Http\Controllers\CommentAnswer::class, 'destroyCommentAnswer'])->name('destroy.comment.answer')->middleware('auth');

Route::post('/store-comment-like', [\App\Http\Controllers\CommentLikeDislikeDataController::class, 'storeCommentLike'])->name('store.comment.like')->middleware('auth');

Route::post('/visited-historical-site', [\App\Http\Controllers\HistoricalSitesController::class, 'visitedHistoricalSite'])->name('visited.historical.site')->middleware('auth');
Route::post('/rating-historical-site', [\App\Http\Controllers\HistoricalSitesController::class, 'ratingHistoricalSite'])->name('rating.historical.site')->middleware('auth');

Route::get('/visited-historical-sites', [\App\Http\Controllers\HistoricalSitesController::class, 'visitedHistoricalSites'])->name('visited.historical.sites')->middleware('auth');

Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/index', [\App\Http\Controllers\Admin\AdminController::class, 'index'])->name('index');

    Route::get('/show-users', [\App\Http\Controllers\Admin\AdminController::class, 'showUsers'])->name('show.users');
    Route::get('/set-user-role/{user}', [\App\Http\Controllers\Admin\AdminController::class, 'setUserRole'])->name('set.user.role');

    Route::post('/update-user-role/{user}', [\App\Http\Controllers\Admin\AdminController::class, 'updateUserRole'])->name('update.user.role');

    Route::get('/create-role', [\App\Http\Controllers\Admin\AdminController::class, 'createRole'])->name('create.role');
    Route::post('/store-role', [\App\Http\Controllers\Admin\AdminController::class, 'storeRole'])->name('store.role');

    Route::get('/create-permission-role/{role}', [\App\Http\Controllers\Admin\AdminController::class, 'createPermissionRole'])->name('create.permission.role');
    Route::post('/update-permission-role/{role}', [\App\Http\Controllers\Admin\AdminController::class, 'updatePermissionRole'])->name('update.permission.role');

    Route::get('/show-historical-sites', [\App\Http\Controllers\Admin\AdminController::class, 'showHistoricalSites'])->name('show.historical.sites');
    Route::get('/show-historical-site/{historicalSite}', [\App\Http\Controllers\Admin\AdminController::class, 'showHistoricalSite'])->name('show.historical.site');

    Route::get('/create-historical-site', [\App\Http\Controllers\Admin\AdminController::class, 'createHistoricalSite'])->name('create.historical.site');
    Route::post('/store-historical-site', [\App\Http\Controllers\Admin\AdminController::class, 'storeHistoricalSite'])->name('store.historical.site');

    Route::get('/edit-historical-site/{historicalSite}', [\App\Http\Controllers\Admin\AdminController::class, 'editHistoricalSite'])->name('edit.historical.site');
    Route::patch('/update-historical-site/{historicalSite}', [\App\Http\Controllers\Admin\AdminController::class, 'updateHistoricalSite'])->name('update.historical.site');
});


require __DIR__.'/auth.php';
