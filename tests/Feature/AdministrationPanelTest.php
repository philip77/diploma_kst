<?php

namespace Tests\Feature;

use App\Models\HistoricalSites;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use App\Models\UserInfo;
use App\Services\AdminService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AdministrationPanelTest extends TestCase
{
    use RefreshDatabase;

    private User $admin;
    private User $user;

    private Role $adminRole;

    protected function setUp(): void
    {
        parent::setUp();

        $permissionsArray = (new AdminService())->getAdminControllerMethodsData();
        $this->adminRole = Role::factory(1)->create(['name' => 'Admin'])->first();

        $dateTimeNow = (new \DateTime())->format('Y-m-d H:i:s');

        foreach ($permissionsArray as $permission) {
            $permission = Permission::factory(1)->create(['controller' => $permission->class, 'action' => $permission->name]);
            $query = 'INSERT INTO permission_role (permission_id, role_id, created_at, updated_at) VALUES (?, ?, ?, ?)';
            DB::insert($query, [$permission[0]->id, $this->adminRole->id, $dateTimeNow, $dateTimeNow]);
        }

        $this->admin = User::factory(1)->create()->first();
        UserInfo::factory()->create(['user_id' => $this->admin->id]);

        $query = 'INSERT INTO role_user (role_id, user_id, created_at, updated_at) VALUES (?, ?, ?, ?)';
        DB::insert($query, [$this->adminRole->id, $this->admin->id, $dateTimeNow, $dateTimeNow]);

        $this->user = User::factory()->create();
        UserInfo::factory()->create(['user_id' => $this->user->id]);
    }

    public function test_user_with_permissions_in_admin_panel(): void
    {
        $response = $this->actingAs($this->admin)->get('/admin/index');

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_panel(): void
    {
        $response = $this->actingAs($this->user)->get('/admin/index');

        $response->assertStatus(302);
        $response->assertRedirect('dashboard');
    }

    public function test_user_with_permissions_in_admin_show_user(): void
    {
        $response = $this->actingAs($this->admin)->get('/admin/show-users');

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_show_user(): void
    {
        $response = $this->actingAs($this->user)->get('/admin/show-users');

        $response->assertStatus(302);
        $response->assertRedirect('dashboard');
    }

    public function test_user_with_permissions_in_admin_set_user_role(): void
    {
        $response = $this->actingAs($this->admin)->get("/admin/set-user-role/{$this->user->id}");

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_set_user_role(): void
    {
        $response = $this->actingAs($this->user)->get("/admin/set-user-role/{$this->user->id}");

        $response->assertStatus(302);
        $response->assertRedirect('dashboard');
    }

    public function test_add_role_to_user(): void
    {
        $roleData = [
            'role' => $this->adminRole->id
        ];

        $roleDataMustExist = [
            'role_id' => $this->adminRole->id,
            'user_id' => $this->user->id
        ];

        $response = $this->actingAs($this->admin)->post("/admin/update-user-role/{$this->user->id}", $roleData);

        $response->assertStatus(302);
        $response->assertRedirect("/admin/set-user-role/{$this->user->id}");

        $this->assertDatabaseHas('role_user', $roleDataMustExist);
    }

    public function test_user_with_permissions_in_admin_create_role(): void
    {
        $response = $this->actingAs($this->admin)->get('/admin/create-role');

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_create_role(): void
    {
        $response = $this->actingAs($this->user)->get('/admin/create-role');

        $response->assertStatus(302);
        $response->assertRedirect('/admin/index');
    }

    public function test_add_role(): void
    {
        $role = [
            'name' => 'Moderator'
        ];

        $response = $this->actingAs($this->admin)->post('/admin/store-role', $role);

        $response->assertStatus(302);
        $response->assertRedirect('/admin/create-role');

        $this->assertDatabaseHas('roles', $role);
        $latestRole = Role::where('name', 'Moderator')->first();;

        $this->assertEquals($role['name'], $latestRole->name);
    }

    public function test_user_with_permissions_in_admin_create_permission_role(): void
    {
        $response = $this->actingAs($this->admin)->get("/admin/create-permission-role/{$this->adminRole->id}");

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_create_permission_role(): void
    {
        $response = $this->actingAs($this->user)->get("/admin/create-permission-role/{$this->adminRole->id}");

        $response->assertStatus(302);
        $response->assertRedirect('/admin/index');
    }

    public function test_store_permission_role(): void
    {
        $moderatorRole = Role::create(['name' => 'Moderator']);
        $permission = Permission::first();

        $permissions = [
            'class' => 'App\Http\Controllers\Admin\AdminController',
            'actions' => [$permission->action]
        ];

        $permissionRoleMustExist = [
            'permission_id' => $permission->id,
            'role_id' => $moderatorRole->id
        ];

        $response = $this->actingAs($this->admin)->post("/admin/update-permission-role/{$moderatorRole->id}", $permissions);

        $response->assertStatus(302);
        $response->assertRedirect("/admin/create-permission-role/{$moderatorRole->id}");
        $this->assertDatabaseHas('permission_role', $permissionRoleMustExist);
    }

    public function test_user_with_permissions_in_admin_show_historical_sites(): void
    {
        $response = $this->actingAs($this->admin)->get('/admin/show-historical-sites');

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_show_historical_sites(): void
    {
        $response = $this->actingAs($this->user)->get('/admin/show-historical-sites');

        $response->assertStatus(302);
        $response->assertRedirect('/dashboard');
    }

    public function test_user_with_permissions_in_admin_create_historical_site(): void
    {
        $response = $this->actingAs($this->admin)->get('/admin/create-historical-site');

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_create_historical_site(): void
    {
        $response = $this->actingAs($this->user)->get('/admin/create-historical-site');

        $response->assertStatus(302);
        $response->assertRedirect('/admin/show-historical-sites');
    }

    public function test_user_with_permissions_in_admin_show_historical_site(): void
    {
        $historicalSiteId = HistoricalSites::factory()->create()->first()->id;
        $response = $this->actingAs($this->admin)->get("/admin/show-historical-site/{$historicalSiteId}");

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_show_historical_site(): void
    {
        $historicalSiteId = HistoricalSites::factory()->create()->first()->id;
        $response = $this->actingAs($this->user)->get("/admin/show-historical-site/{$historicalSiteId}");

        $response->assertStatus(302);
        $response->assertRedirect('/dashboard');
    }

    public function test_user_with_permissions_in_admin_edit_historical_site(): void
    {
        $historicalSiteId = HistoricalSites::factory()->create()->first()->id;
        $response = $this->actingAs($this->admin)->get("/admin/edit-historical-site/{$historicalSiteId}");

        $response->assertStatus(200);
    }

    public function test_user_without_permissions_in_admin_edit_historical_site(): void
    {
        $historicalSiteId = HistoricalSites::factory()->create()->first()->id;
        $response = $this->actingAs($this->user)->get("/admin/edit-historical-site/{$historicalSiteId}");

        $response->assertStatus(302);
        $response->assertRedirect('/admin/show-historical-sites');
    }

    public function test_create_historical_site()
    {
        $historicalSite = [
            'name' => 'Colosseum',
            'data' => 'Colosseum Information',
            'confirmed' => 1
        ];

        $response = $this->actingAs($this->admin)->post('/admin/store-historical-site', $historicalSite);

        $response->assertStatus(302);
        $response->assertRedirect('/admin/show-historical-sites');

        $this->assertDatabaseHas('historical_sites', $historicalSite);
        $latestHistoricalSite = HistoricalSites::latest()->first();

        $this->assertEquals($historicalSite['name'], $latestHistoricalSite->name);
        $this->assertEquals($historicalSite['data'], $latestHistoricalSite->data);
    }

    public function test_edit_historical_site()
    {
        $historicalSite = HistoricalSites::factory()->create()->first();

        $response = $this->actingAs($this->admin)->patch("/admin/update-historical-site/$historicalSite->id", [
            'name' => 'Edit Name',
            'data' => 'Edit Data'
        ]);

        $editHistoricalSite = HistoricalSites::where('id', $historicalSite->id)->first();

        $response->assertStatus(302);
        $response->assertRedirect("/admin/show-historical-site/$historicalSite->id");

        $this->assertDatabaseHas('historical_sites', ['name' => $editHistoricalSite->name, 'data' => $editHistoricalSite->data]);
    }

    public function test_validation_error_on_edit_historical_site()
    {
        $historicalSite = HistoricalSites::factory()->create()->first();
        $newHistoricalSite = [
            'name' => '',
            'data' => 'Edit Data'
        ];

        $response = $this->actingAs($this->admin)->patch("/admin/update-historical-site/$historicalSite->id", $newHistoricalSite);

        $response->assertStatus(302);
        $response->assertSessionHasErrors(['name']);
        $this->assertDatabaseMissing('historical_sites', $newHistoricalSite);
    }
}
