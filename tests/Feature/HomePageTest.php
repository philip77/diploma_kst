<?php

namespace Tests\Feature;

use App\Models\HistoricalSites;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomePageTest extends TestCase
{
    use RefreshDatabase;

    private Collection $historicalSites;
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->historicalSites = HistoricalSites::factory(10)->create();
        $this->user = User::factory()->create();
    }

    /**
     * A basic test example.
     */
    public function test_the_application_returns_a_successful_response(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_login_user_to_see_home_page(): void
    {
        $response = $this->actingAs($this->user)->get('/');

        $response->assertStatus(302);
        $response->assertRedirect('/dashboard');
    }

    public function test_application_home_page_contains_heading(): void
    {
        $response = $this->get('/');

        $response->assertSee(__('Welcome in Historical Sites!'));
    }

    public function test_application_home_page_show_historical_sites()
    {
        $response = $this->get('/');

        $response->assertOk();
        $response->assertViewHas('historicalSites', function ($collection) {
            return $collection->contains($this->historicalSites->last());
        });
    }

    public function test_pagination_table_doesnt_contains_last_historical_site()
    {
        $response = $this->get('/');

        $response->assertOk();
        $response->assertViewHas('historicalSites', function ($collection) {
            return !$collection->contains($this->historicalSites->first());
        });
    }
}
