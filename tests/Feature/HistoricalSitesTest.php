<?php

namespace Tests\Feature;

use App\Models\HistoricalSites;
use App\Models\User;
use App\Models\UserInfo;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HistoricalSitesTest extends TestCase
{
    use RefreshDatabase;
    private HistoricalSites $historicalSite;
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        UserInfo::factory()->create(['user_id' => $this->user->id]);
        $this->historicalSite = HistoricalSites::factory()->create()->first();
    }
    /**
     * A basic feature test example.
     */
    public function test_login_user_to_see_dashboard(): void
    {
        $response = $this->actingAs($this->user)->get('/dashboard');

        $response->assertStatus(200);
    }

    public function test_not_login_user_to_be_redirect(): void
    {
        $response = $this->get('/dashboard');

        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_login_user_to_see_home(): void
    {
        $response = $this->actingAs($this->user)->get('/dashboard');

        $response->assertStatus(200);
    }

    public function test_login_user_to_see_historical_site_show(): void
    {
        $response = $this->actingAs($this->user)->get("/historical-site-show/{$this->historicalSite->id}");

        $response->assertStatus(200);
    }

    public function test_not_login_user_to_see_historical_site_show(): void
    {
        $response = $this->get("/historical-site-show/{$this->historicalSite->id}");

        $response->assertStatus(302);
        $response->assertRedirect('login');
    }

    public function test_rating_historical_site(): void
    {
        $ratingHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'rating' => 2
        ];

        $response = $this->actingAs($this->user)->postJson('/rating-historical-site', $ratingHistoricalSite);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully set score for historical site!']);
        $this->assertDatabaseHas('historical_site_score', $ratingHistoricalSite);
    }

    public function test_error_on_store_rating_historical_site(): void
    {
        $ratingHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'rating' => 10
        ];

        $response = $this->actingAs($this->user)->postJson('/rating-historical-site', $ratingHistoricalSite);

        $response->assertStatus(422);
        $response->assertJsonFragment(['message' => 'The selected rating is invalid.']);
        $this->assertDatabaseMissing('historical_site_score', $ratingHistoricalSite);
    }

    public function test_error_no_permission_on_store_rating_historical_site(): void
    {
        $ratingHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'rating' => 2
        ];

        $response = $this->postJson('/rating-historical-site', $ratingHistoricalSite);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->assertDatabaseMissing('historical_site_score', $ratingHistoricalSite);
    }

    public function test_visited_historical_site(): void
    {
        $visitedHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'is_visited' => 1
        ];

        $response = $this->actingAs($this->user)->postJson('/visited-historical-site', $visitedHistoricalSite);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add is visited flag for historical site!']);
        $this->assertDatabaseHas('historical_site_user', $visitedHistoricalSite);
    }

    public function test_visited_historical_site_page_work(): void
    {
        $visitedHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'is_visited' => 1
        ];

        $response = $this->actingAs($this->user)->postJson('/visited-historical-site', $visitedHistoricalSite);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add is visited flag for historical site!']);
        $this->assertDatabaseHas('historical_site_user', $visitedHistoricalSite);

        $response = $this->get('/visited-historical-sites');
        $response->assertSee($this->historicalSite->name);
    }

    public function test_error_on_store_visited_historical_site(): void
    {
        $visitedHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'is_visited' => 10
        ];

        $response = $this->actingAs($this->user)->postJson('/visited-historical-site', $visitedHistoricalSite);

        $response->assertStatus(422);
        $response->assertJsonFragment(['message' => 'The selected is visited is invalid.']);
        $this->assertDatabaseMissing('historical_site_user', $visitedHistoricalSite);
    }

    public function test_error_no_permission_on_store_visited_historical_site(): void
    {
        $visitedHistoricalSite = [
            'historical_site_id' => $this->historicalSite->id,
            'user_id' => $this->user->id,
            'is_visited' => 10
        ];

        $response = $this->postJson('/visited-historical-site', $visitedHistoricalSite);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->assertDatabaseMissing('historical_site_user', $visitedHistoricalSite);
    }
}
