<?php

namespace Tests\Feature;

use App\Enum\CommentType;
use App\Enum\CommentValue;
use App\Models\CommentAnswers;
use App\Models\Comments;
use App\Models\HistoricalSites;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentsTest extends TestCase
{
    use RefreshDatabase;

    private HistoricalSites $historicalSite;
    private User $user;
    private Comments $comment;
    private CommentAnswers $commentAnswer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->historicalSite = HistoricalSites::factory()->create()->first();
        $this->comment = Comments::factory()->create(['historical_site_id' => $this->historicalSite->id, 'user_id' => $this->user->id])->first();
        $this->commentAnswer = CommentAnswers::factory()->create(['comment_id' => $this->comment->id, 'user_id' => $this->user->id])->first();
    }

    public function test_store_comment(): void
    {
        $comment = [
            'comment' => 'Test Comment',
            'historical_site_id' => $this->historicalSite->id
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment', $comment);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add comment!']);
        $this->assertDatabaseHas('comments', $comment);
    }

    public function test_error_on_store_comment(): void
    {
        $comment = [
            'comment' => '',
            'historical_site_id' => $this->historicalSite->id
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment', $comment);

        $response->assertStatus(422);
        $response->assertJsonFragment(['message' => 'The comment field is required.']);
        $this->assertDatabaseMissing('comments', $comment);
    }

    public function test_error_no_permission_on_store_comment(): void
    {
        $comment = [
            'comment' => 'Test Comment',
            'historical_site_id' => $this->historicalSite->id
        ];

        $response = $this->postJson('/store-comment', $comment);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->assertDatabaseMissing('comments', $comment);
    }

    public function test_store_comment_answer(): void
    {
        $commentAnswer = [
            'answer' => 'Test Answer',
            'comment_id' => $this->comment->id
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment-answer', $commentAnswer);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add answer for comment!']);
        $this->assertDatabaseHas('comment_answers', $commentAnswer);
    }

    public function test_error_on_store_comment_answer(): void
    {
        $commentAnswer = [
            'answer' => '',
            'comment_id' => $this->comment->id
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment-answer', $commentAnswer);

        $response->assertStatus(422);
        $response->assertJsonFragment(['message' => 'The answer field is required.']);
        $this->assertDatabaseMissing('comment_answers', $commentAnswer);
    }

    public function test_error_no_permission_on_store_comment_answer(): void
    {
        $commentAnswer = [
            'answer' => 'Test Answer',
            'comment_id' => $this->comment->id
        ];

        $response = $this->postJson('/store-comment-answer', $commentAnswer);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->assertDatabaseMissing('comments', $commentAnswer);
    }

    public function test_delete_comment()
    {
        $comment = [
            'id' => $this->comment->id
        ];

        $commentAfterDelete = [
            'id' => $this->comment->id,
            'deleted' => 1
        ];

        $response = $this->actingAs($this->user)->postJson('/destroy-comment', $comment);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully delete comment!']);
        $this->assertDatabaseHas('comments', $commentAfterDelete);
    }

    public function test_error_no_permissions_delete_comment()
    {
        $comment = [
            'id' => $this->comment->id
        ];

        $commentNotDeleted = [
            'id' => $this->comment->id,
            'deleted' => 0
        ];

        $response = $this->postJson('/destroy-comment', $comment);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->assertDatabaseHas('comments', $commentNotDeleted);
    }

    public function test_error_user_is_not_owner_delete_comment()
    {
        $comment = [
            'id' => $this->comment->id
        ];

        $commentAfterDelete = [
            'id' => $this->comment->id,
            'deleted' => 0
        ];

        $user = User::factory()->create();

        $response = $this->actingAs($user)->postJson('/destroy-comment', $comment);

        $response->assertStatus(422);
        $response->assertJsonFragment(['message' => 'The selected id is invalid.']);
        $this->assertDatabaseHas('comments', $commentAfterDelete);
    }

    public function test_delete_comment_answer()
    {
        $commentAnswer = [
            'id' => $this->commentAnswer->id
        ];

        $commentAnswerAfterDelete = [
            'id' => $this->commentAnswer->id,
            'deleted' => 1
        ];

        $response = $this->actingAs($this->user)->postJson('/destroy-comment-answer', $commentAnswer);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully delete answer!']);
        $this->assertDatabaseHas('comment_answers', $commentAnswerAfterDelete);
    }

    public function test_error_no_permissions_delete_comment_answer()
    {
        $commentAnswer = [
            'id' => $this->commentAnswer->id
        ];

        $commentAnswerNotDeleted = [
            'id' => $this->commentAnswer->id,
            'deleted' => 0
        ];

        $response = $this->postJson('/destroy-comment-answer', $commentAnswer);

        $response->assertStatus(401);
        $response->assertJsonFragment(['message' => 'Unauthenticated.']);
        $this->assertDatabaseHas('comments', $commentAnswerNotDeleted);
    }

    public function test_error_user_is_not_owner_delete_comment_answer()
    {
        $commentAnswer = [
            'id' => $this->commentAnswer->id
        ];

        $commentAnswerAfterDelete = [
            'id' => $this->commentAnswer->id,
            'deleted' => 0
        ];

        $user = User::factory()->create();

        $response = $this->actingAs($user)->postJson('/destroy-comment-answer', $commentAnswer);

        $response->assertStatus(422);
        $response->assertJsonFragment(['message' => 'The selected id is invalid.']);
        $this->assertDatabaseHas('comments', $commentAnswerAfterDelete);
    }

    public function test_store_comment_like()
    {
        $commentLike = [
            'comment_answer_id' => null,
            'comment_id' => $this->comment->id,
            'user_id' => $this->user->id,
            'comment_value' => CommentValue::LIKE,
            'comment_type' => CommentType::COMMENT
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment-like', $commentLike);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add like/dislike for comment!']);
        $this->assertDatabaseHas('comment_like_dislike_data', $commentLike);
    }

    public function test_store_comment_dislike()
    {
        $commentDislike = [
            'comment_answer_id' => null,
            'comment_id' => $this->comment->id,
            'user_id' => $this->user->id,
            'comment_value' => CommentValue::DISLIKE,
            'comment_type' => CommentType::COMMENT
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment-like', $commentDislike);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add like/dislike for comment!']);
        $this->assertDatabaseHas('comment_like_dislike_data', $commentDislike);
    }

    public function test_store_comment_answer_like()
    {
        $commentAnswerLike = [
            'comment_answer_id' => $this->commentAnswer->id,
            'comment_id' => $this->comment->id,
            'user_id' => $this->user->id,
            'comment_value' => CommentValue::LIKE,
            'comment_type' => CommentType::ANSWER
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment-like', $commentAnswerLike);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add like/dislike for comment!']);
        $this->assertDatabaseHas('comment_like_dislike_data', $commentAnswerLike);
    }

    public function test_store_comment_answer_dislike()
    {
        $commentAnswerDislike = [
            'comment_answer_id' => $this->commentAnswer->id,
            'comment_id' => $this->comment->id,
            'user_id' => $this->user->id,
            'comment_value' => CommentValue::DISLIKE,
            'comment_type' => CommentType::ANSWER
        ];

        $response = $this->actingAs($this->user)->postJson('/store-comment-like', $commentAnswerDislike);

        $response->assertStatus(201);
        $response->assertJsonFragment(['success' => 'Successfully add like/dislike for comment!']);
        $this->assertDatabaseHas('comment_like_dislike_data', $commentAnswerDislike);
    }
}
