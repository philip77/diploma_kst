<?php

namespace Tests\Feature;

use App\Enum\Gender;
use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use MyCLabs\Enum\Enum;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    private User $user;
    private UserInfo $userInfo;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
        $this->userInfo = UserInfo::factory()->create(['user_id' => $this->user->id]);
    }

    public function test_profile_page_is_displayed(): void
    {
        $response = $this
            ->actingAs($this->user)
            ->get('/profile');

        $response->assertOk();
    }

    public function test_profile_information_can_be_updated(): void
    {
        $response = $this
            ->actingAs($this->user)
            ->patch('/profile', [
                'username' => 'Test User',
                'email' => 'test@example.com',
            ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertRedirect('/profile');

        $this->user->refresh();

        $this->assertSame('Test User', $this->user->username);
        $this->assertSame('test@example.com', $this->user->email);
        $this->assertNull($this->user->email_verified_at);
    }

    public function test_email_verification_status_is_unchanged_when_the_email_address_is_unchanged(): void
    {
        $response = $this
            ->actingAs($this->user)
            ->patch('/profile', [
                'username' => 'Test User',
                'email' => $this->user->email,
            ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertRedirect('/profile');

        $this->assertNotNull($this->user->refresh()->email_verified_at);
    }

    public function test_user_can_delete_their_account(): void
    {
        $response = $this
            ->actingAs($this->user)
            ->delete('/profile', [
                'password' => 'password',
            ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertRedirect('/');

        $this->assertGuest();
        $this->assertNull($this->user->fresh());
    }

    public function test_correct_password_must_be_provided_to_delete_account(): void
    {
        $response = $this
            ->actingAs($this->user)
            ->from('/profile')
            ->delete('/profile', [
                'password' => 'wrong-password',
            ]);

        $response
            ->assertSessionHasErrorsIn('userDeletion', 'password')
            ->assertRedirect('/profile');

        $this->assertNotNull($this->user->fresh());
    }

    public function test_update_profile_info_information(): void
    {
        $profileInfo = [
            'first_name' => 'Philip',
            'middle_name' => 'G',
            'last_name' => 'Popov',
            'gender' => Gender::MALE->value,
            'birth_date' => '05/08/2023'
        ];

        $response = $this->actingAs($this->user)->patch("/profile-info/{$this->userInfo->first()->id}", $profileInfo);

        $profileInfo['birth_date'] = (new \DateTime($profileInfo['birth_date']))->format('Y-m-d');

        $response->assertStatus(302);
        $response->assertRedirect('/profile');
        $this->assertDatabaseHas('user_infos', $profileInfo);
    }

    public function test_error_on_invalid_data_update_profile_info_information(): void
    {
        $profileInfo = [
            'first_name' => 'Philip',
            'middle_name' => 'G',
            'last_name' => 'Popov',
            'gender' => 'invalid',
            'birth_date' => '05/08/2023'
        ];

        $response = $this->actingAs($this->user)->patch("/profile-info/{$this->userInfo->first()->id}", $profileInfo);

        $profileInfo['birth_date'] = (new \DateTime($profileInfo['birth_date']))->format('Y-m-d');

        $response->assertStatus(302);
        $response->assertRedirect('/');
        $this->assertDatabaseMissing('user_infos', $profileInfo);
    }
}
